--- @type Mq
local mq = require('mq')

-- Libs
local utils = require('quickhud.utils')
local log = require('quickhud.lib.Write')
local LIP = require('quickhud.lib.LIP')
local ICON = require('quickhud.lib.icons')
dofile('quickhud/lib/persistence.lua')

local linq = require("quickhud.lib.lazylualinq.linq")

-- Exported Module
local services = {}

-- Nav Service
local Nav = {}
function Nav:loadNavData()
    configPath = mq.TLO.MacroQuest.Path():gsub('\\', '/')
    iniFile = '/config/MQ2Nav.ini'
    filePath = configPath..iniFile

    return utils.getCachedData("mq2nav.ini", function() return self:ReadIniFile(filePath) end, 5)
end

function Nav:ReadIniFile(filePath)
    if utils.fileExists(filePath) then
        return LIP.load(filePath)
    end

    log.Debug(string.format("%s FILE NOT FOUND", filePath))
end

function Nav:RecordWaypoint(waypointName, coords)
    local command = string.format("/nav rwp %s", waypointName)
    mq.cmd(command)
    mq.cmd("/nav save")
    local recordedMessage = string.format("Recorded loc %s as waypoint '%s'", coords, waypointName)
    log.Info(recordedMessage)
    mq.cmd("/popup "..recordedMessage)
    utils.removeCachedItem("mq2nav.ini")
end

function Nav:GetWaypointsForZone(zoneShortName)
    local navIniData = self:loadNavData()

    zoneShortName = string.lower(zoneShortName)
    zoneShortName = zoneShortName:gsub("%_.+", "")

    local navDataNormalized = {}

    for k,v in pairs(navIniData) do
        navDataNormalized[string.lower(k)] = v
    end

    local waypoints = navDataNormalized[zoneShortName]

    return waypoints
end

function Nav:NavigateToLoc(location)
    log.Debug(string.format("should now navigate to loc: %s", location))
    mq.cmd(string.format("/nav loc %s", location))
end

function Nav:NavigateToWaypoint(waypointName)
    log.Debug("should now navigate to waypoint: " .. waypointName)
    mq.cmd("/nav load")
    mq.cmd(string.format("/nav wp %s", waypointName))
end

function Nav:NavigateToId(Id, stopDistance)
    stopDistance = stopDistance or 10
    log.Debug("should now navigate to id: " .. Id .. " & stop " .. stopDistance .. " away")
    mq.cmd(string.format("/nav id %s | dist=%s", Id, stopDistance))
end

function Nav:NavigateToWaypointLoc(location)
    log.Debug("should now navigate to loc: " .. location)
    -- MQ2Nav.ini stores coordinates with the first two vectors transposed from the way it interprets them via /nav loc
    local coords = utils.split(location)
    mq.cmd(string.format("/nav loc %s %s %s", coords[2], coords[1], coords[3]))
end

services.Nav = Nav

function curryFilter(filter)
    return function() return getZonePopulationDataForFilter(filter) end
end

function resolveSpawnData(spawn)
    local spawnData = {
        name = spawn.CleanName() or "",
        distance = string.format("%.2f", spawn.Distance3D() or 0),
        level = spawn.Level() or 0,
        race = spawn.Race() or "",
        class = spawn.Class.ShortName() or "",
        location = spawn.LocYXZ() or "",
        guild = spawn.Guild() or "Unguilded",
        named = spawn.Named(),
        heading = spawn.Heading() or "",
        conColor = spawn.ConColor() or "BLACK",
        los = spawn.LineOfSight(),
        pctHps = spawn.PctHPs() or 0,
        id = spawn.ID() or 0,
        spawnAccessor = function() return spawn end
    }

    return spawnData
end

function buildSpawnDataAccessor(spawn)
    return function() return resolveSpawnData(spawn) end
end

function getZonePopulationDataForFilter(filter, limit)
    limit = limit or 200
    local spawnCollection = {}

    local i = 1
    while mq.TLO.NearestSpawn(i..filter)() and i <= limit do
        local spawn = mq.TLO.NearestSpawn(i..filter)
        local spawnId = spawn.ID()

        local spawnData = {
            id = spawnId,
            spawn = spawn,
            spawnDataAccessor = buildSpawnDataAccessor(spawn)
        }

        table.insert(spawnCollection, spawnData)

        i = i + 1
    end

    return spawnCollection
end

-- Zone Data Service
local ZoneData = {}

function ZoneData:GetPlayers()
    return utils.getCachedData("zone-player-data", curryFilter(", pc"), 0.15)
end

function ZoneData:GetNpcs()
    return utils.getCachedData("zone-npc-data", curryFilter(", npc"), 0.15)
end

function ZoneData:GetNamed()
    return utils.getCachedData("zone-named-data", curryFilter(", npc named"), 0.15)
end

function ZoneData:GetGuilds()
    return utils.getCachedData("zone-guilds-data", function()
        local players = self:GetPlayers()
        local guildData = {}

        for index, player in ipairs(players) do
            local guild = player.spawn.Guild() or "Unguilded"
            if nil == guildData[guild] then
                guildData[guild] = {}
            end
            table.insert(guildData[guild], player)
        end

        local guilds = linq.from(guildData)
            :select(function(players, guild) return {guild = guild, count = #players, players = players} end)
            :orderByDescending(function(record) return record.count end)
            :thenBy(function(record) return record.guild end)
            :toArray()

        return guilds
    end, 0.15)
end

services.ZoneData = ZoneData

local fakeCharacter = {
    Invis = function(invisMode) return (invisMode and (function() return true end)) or true end,
    Levitating = function() return true end,
    Level = function() return 107 end,
    Class = { Name = function() return "Cleric" end, ShortName = function() return "CLR" end },
    X = function() return 120.25 end,
    Y = function() return 274.8 end,
    Z = function() return 62.36 end,
    Speed = function() return 120.25 end,
    DisplayName = function() return "Toon D`Fake" end,
    Race = function() return "Druish Princess" end,
    Distance3D = function() return 120.25 end,
    PctHPs = function() return 100 end,
    ID = function() return 1000 end,
    SeeInvis = function() return false end,
    Aggressive = function() return true end,
    CombatState = function() return "RESTING" end,
    Subscription = function() return "SILVER" end,
    ConColor = function() return "GREEN" end,
    LineOfSight = function() return true end,
    Heading = function() return "NW" end,
    Named = function() return false end
}

-- Player Actions
local PlayerActions = {}

local LEVITATE = "levitate"
local LEVITATION = "levitation"

function isLevitationSpell(spell)
    return string.lower(spell.Subcategory()) == LEVITATE
        or string.find(string.lower(spell.Name()), LEVITATION, 1, true)
        or string.find(string.lower(spell.Description()), LEVITATION, 1, true)
end

function PlayerActions:RemoveLevitationEffects()
    for buffIndex = 1,50 do
        local buff = mq.TLO.Me.Buff(buffIndex)
        if nil ~= buff.ID() then
            if isLevitationSpell(buff.Spell) then
                log.Debug(string.format("Buff %s at index %s is of type levitation, removing", buff.Name(), buffIndex))
                buff.Remove()
            end
        end
    end
end

function PlayerActions:TargetById(id)
    mq.cmd(string.format("/target id %s", id))
end

function PlayerActions:TargetMyself()
    mq.cmd(string.format("/target id %s", mq.TLO.Me.ID()))
end

services.PlayerActions = PlayerActions

-- Mock Service
services.MockData = {
    FakeCharacterAccessor = function() return fakeCharacter end,
    GenerateMockMacro = function(name, paused)
        return function() return { Name = function() return name end, Paused = function() return paused end } end
    end
}

local classStatMap = {
    endurance = utils.set({"BER","BST","MNK","PAL","RNG","ROG","SHD","WAR"}),
    mana = utils.set({"BRD","BST","CLR","DRU","ENC","MAG","NEC","PAL","RNG","SHD","SHM","WIZ"})
}

-- EQ Data Service
local EQData = {}

function EQData:classHasMana(classShortName)
    return (nil ~= classStatMap.mana[classShortName])
end

function EQData:classHasEndurance(classShortName)
    return (nil ~= classStatMap.endurance[classShortName])
end

services.EQData = EQData

local AppSettings = {}

AppSettings.DefaultSettingsPath = table.concat({mq.luaDir, 'quickhud', 'settings-template.lua'}, '/'):gsub('\\', '/')
AppSettings.SharedSettingsFileName = "settings.lua"

AppSettings.DefaultWindowTitleFormatString = '%s Quick HUD##%sdefault'

function AppSettings:GetMainWindowTitle()
    return string.format(AppSettings.DefaultWindowTitleFormatString, ICON.MD_INSERT_CHART, mq.TLO.Me.CleanName())
end

function AppSettings:EnumerateExistingConfigurations()
    return utils.getCachedData("config data", function()
        local configPath = self:GetConfigDirectory()
        log.Debug(string.format("looking in %s for config files", configPath))
        local settingsFiles = utils.scandir(configPath)
        local sharedSettingsFileEntryIndex = utils.findIndex(settingsFiles, AppSettings.SharedSettingsFileName)
        if nil ~= sharedSettingsFileEntryIndex then
            table.remove(settingsFiles, sharedSettingsFileEntryIndex)
        end
        return settingsFiles
    end, 30)
end

function AppSettings:LoadRequiredPlugins()
    if not mq.TLO.Plugin('mq2nav').IsLoaded() then
        log.Info("The plugin MQ2Nav is not loaded and is required for this utility. Loading it now.")
        mq.cmd('/plugin mq2nav noauto')
    end
end

function AppSettings:GetConfigDirectory()
    return string.format('%s\\quickhud', mq.configDir):gsub('\\', '/'):lower()
end

function AppSettings:GetIndividualSettingsFilePath()
    return table.concat({self:GetConfigDirectory(), self:GetIndividualSettingsFileName()}, '/')
end

function AppSettings:GetSharedSettingsFilePath()
    return table.concat({self:GetConfigDirectory(), self.SharedSettingsFileName}, '/')
end

function AppSettings:LoadAppSettings()
    local individualSettings = self:LoadIndividualSettings()
    if nil ~= individualSettings and nil ~= next(individualSettings) and individualSettings.useIndividualSettings then
        return individualSettings
    end

    local sharedSettings = self:LoadSharedSettings()
    if nil ~= sharedSettings and nil ~= next(sharedSettings) then
        return sharedSettings
    end

    return self:InitializeDefaultSettings()
end

function AppSettings:InitializeDefaultSettings()
    log.Debug(string.format("Copying Shared Settings from Default Settings File: %s", self.DefaultSettingsPath))
    local defaultSettings = require('quickhud.settings-template')
    utils.createDirectory(self:GetConfigDirectory())
    persistence.store(self:GetSharedSettingsFilePath(), defaultSettings)
    return defaultSettings
end

function AppSettings:LoadIndividualSettings()
    local individualSettingsPath = self:GetIndividualSettingsFilePath()
    if utils.fileExists(individualSettingsPath) then
        local settings = assert(loadfile(individualSettingsPath))()
        log.Debug(string.format("Loading Settings from Individual Settings File: %s | version %s", individualSettingsPath, settings.version))
        settings = self:UpgradeSettings(settings)
        return settings
    end
    return nil
end

function AppSettings:LoadSharedSettings()
    local sharedSettingsPath = self:GetSharedSettingsFilePath()
    if utils.fileExists(sharedSettingsPath) then
        log.Debug(string.format("Loading Settings from Shared Settings File: %s", sharedSettingsPath))
        local settings = assert(loadfile(sharedSettingsPath))()
        settings = self:UpgradeSettings(settings)
        return settings
    end
    return nil
end

function AppSettings:GetIndividualSettingsFileName()
    return string.format("%s-%s.lua", mq.TLO.MacroQuest.Server(), mq.TLO.Me.CleanName())
end

function AppSettings:DoSettingsDiffer(a, b)
    if not b or not a or not (b.version == a.version) then
        return true
    end

    if a[1] ~= nil then
        for index, value in ipairs(a) do
            if type(value) == 'table' then
                if not b or self:DoSettingsDiffer(value, b[index]) then
                    return true
                end
            else
                if b[index] ~= value then
                    return true
                end
            end
        end
    else
        for key, value in pairs(a) do
            if type(value) == 'table' then
                if not b or self:DoSettingsDiffer(value, b[key]) then
                    return true
                end
            else
                if b[key] ~= value then
                    return true
                end
            end
        end
    end

    return false
end

function AppSettings:SaveSettings(settings)
    local settingsPath = self:GetSharedSettingsFilePath()
    if settings and settings.useIndividualSettings then
        settingsPath = self:GetIndividualSettingsFilePath()
    end
    persistence.store(settingsPath, settings)
    return settings
end

function AppSettings:CopySettings(fileName)
    log.Info(string.format("Copying Settings from File: %s", fileName))
    local copyPath = table.concat({self:GetConfigDirectory(), fileName}, '/')
    local settings = assert(loadfile(copyPath))()
    return self:UpgradeSettings(settings)
end

function AppSettings:ChangeSavePreference(settings, targetSettings, shared)
    if nil == targetSettings or nil == next(targetSettings) then
        targetSettings = settings
    end

    targetSettings.useIndividualSettings = not shared

    return self:SaveSettings(targetSettings)
end

function AppSettings:UseIndividualSettings(settings)
    return self:ChangeSavePreference(settings, self:LoadIndividualSettings(), false)
end

function AppSettings:UseSharedSettings(settings)
    settings.useIndividualSettings = false
    persistence.store(self:GetIndividualSettingsFilePath(), settings)

    return self:ChangeSavePreference(settings, self:LoadSharedSettings(), true)
end

function AppSettings:ApplyCustomChanges(settings, template, key, value)
    if key == "uiElements" and settings[key] then
        for index, entry in ipairs(settings[key]) do
            if entry.id == 11 then
                entry.title = "Custom Widgets"
            end
        end
    end
end

function AppSettings:UpgradeSettings(settings)
    local settingsTemplate = require('quickhud.settings-template')

    if settings.version < settingsTemplate.version then
        log.Debug(string.format("Updating settings, from version %s to %s", settings.version, settingsTemplate.version))
        -- Patch in new settings
        for key, value in pairs(settingsTemplate) do
            if nil == settings[key] then
                settings[key] = value
            end

            self:ApplyCustomChanges(settings, template, key, value)
        end

        -- Remove deprecated settings
        for key, value in pairs(settings) do
            if nil == settingsTemplate[key] then
                settings[key] = nil
            end
        end

        settings.version = settingsTemplate.version

        log.Debug(string.format("Updated settings, at version %s", settings.version))

        return self:SaveSettings(settings)
    end

    return settings
end

services.AppSettings = AppSettings

return services
