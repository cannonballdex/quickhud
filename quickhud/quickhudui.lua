--- @type Mq
local mq = require('mq')

--- @type ImGui
require 'ImGui'

-- Logging
local log = require('quickhud.lib.Write')

-- Libraries
local ICON = require('quickhud.lib.icons')
local utils = require('quickhud.utils')
local uiComponents = require('quickhud.uihelpers')
local services = require('quickhud.services')
local state = require('quickhud.state')
local COLOR = require('quickhud.lib.color')
local displayCustomElements = require("quickhud.custom-elements-component")
require('quickhud.breakout')

local linq = require("quickhud.lib.lazylualinq.linq")

-- Services
local navService = services.Nav
local zoneDataService = services.ZoneData
local eqDataService = services.EQData
local playerActionsService = services.PlayerActions
local appSettingsService = services.AppSettings

local defaults = {
    pauseMacro = function() mq.cmd("/mqp on") end,
    resumeMacro = function() mq.cmd("/mqp off") end,
    endMacro = function() mq.cmd("/end") end,
    macroAccessor = function() return mq.TLO.Macro end,
    maTargetAccessor = function() return mq.TLO.Me.GroupAssistTarget end,
    targetAccessor = function() return mq.TLO.Target end,
    gmSpawnAccessor = function() return mq.TLO.Spawn("GM") end,
    meAccessor = function() return mq.TLO.Me end
}

local inputState = {
    bookmarkText = ""
}

local TEXT_BASE_WIDTH, _ = ImGui.CalcTextSize("A")
local TEXT_BASE_HEIGHT = ImGui.GetTextLineHeightWithSpacing();
-- ICON.FA_GLASS  -- thirst
-- ICON.FA_CUTLERY  -- hunger

function displayExperienceInfo()
    local divisor = 0
    if state.settings.showExperience then divisor = divisor + 1 end
    if state.settings.showAAExperience then divisor = divisor + 1 end
    if state.settings.showMercAAExperience then divisor = divisor + 1 end

    local continuation = false

    ImGui.BeginGroup()

    x = ImGui.GetContentRegionAvail()

    ImGui.Columns(divisor, "experiencebars")

    if state.settings.showExperience then
        local experiencePercentage = (mq.TLO.Me.PctExp() or 0)
        uiComponents.DrawInfoBar(experiencePercentage / 100, string.format("Exp %.2f%%", experiencePercentage), uiComponents.GetAvailableX() + 4, 16, state.settings.experienceBarColor)
        uiComponents.ToolTip(string.format("%.2f%% experience to next level", 100 - experiencePercentage))
        continuation = true
    end

    if state.settings.showAAExperience then
        if continuation then ImGui.NextColumn() end
        uiComponents.OffsetCursorPosX(-2)
        local experiencePercentage = (mq.TLO.Me.PctAAExp() or 0)
        uiComponents.DrawInfoBar(experiencePercentage / 100, string.format("AA Exp %.2f%%", experiencePercentage), uiComponents.GetAvailableX() + 4, 16, state.settings.experienceBarColor)
        uiComponents.ToolTip(string.format("%.2f%% experience to next AA level", 100 - experiencePercentage))
        continuation = true
    end

    if state.settings.showMercAAExperience then
        if continuation then ImGui.NextColumn() end
        uiComponents.OffsetCursorPosX(-2)
        local experiencePercentage = (mq.TLO.Me.PctMercAAExp() or 0) / 100 -- The merc AA experience is currently returning a percentage with the wrong decimal (or I don't understand the intention)
        uiComponents.DrawInfoBar(experiencePercentage / 100, string.format("Merc AA Exp %.2f%%", experiencePercentage), uiComponents.GetAvailableX() + 4, 16, state.settings.experienceBarColor)
        uiComponents.ToolTip(string.format("%.2f%% experience to next merc AA level", 100 - experiencePercentage))
    end

    ImGui.Columns(1)

    ImGui.EndGroup()

    if state.settings.showExperience or state.settings.showAAExperience or state.settings.showMercAAExperience then
        if state.settings.showLineSeparators then ImGui.Separator() end
    end
end

function displayMonitoredTargetInfo()
    if state.settings.showMyTargetInfo then displayTargetInfo() end
    if state.settings.showMyTargetInfo and state.settings.showMATargetInfo then
        uiComponents.OffsetCursorPosY(-4)
    end
    if state.settings.showMATargetInfo then displayMATargetInfo() end
    uiComponents.OffsetCursorPosY(-4)
    if state.settings.showLineSeparators then ImGui.Separator() end
end

local COMBATSTATE_MAP = {
    COLOR = {
        ACTIVE = function() return state.settings.showStatBackgroundColor and state.settings.activeStateBackgroundColor or state.settings.combatStateDefaultColor end,
        RESTING = function() return state.settings.showStatBackgroundColor and state.settings.restingStateBackgroundColor or state.settings.combatStateDefaultColor end,
        COMBAT = function() return state.settings.showStatBackgroundColor and state.settings.combatStateBackgroundColor or state.settings.combatStateDefaultColor end,
        DEBUFFED = function() return state.settings.showStatBackgroundColor and state.settings.debuffedStateBackgroundColor or state.settings.combatStateDefaultColor end,
        COOLDOWN = function() return state.settings.showStatBackgroundColor and state.settings.cooldownStateBackgroundColor or state.settings.combatStateDefaultColor end,
        DEFAULT = function() return state.settings.combatStateDefaultColor end
    },

    INFO = {
        COMBAT = "You are currently in a combat state and can not rest yet.",
        DEBUFFED = "You can't rest now. You need to be cleansed before the cooldown will start",
        COOLDOWN = "You are cooling down. OOC regen is ready when you are done cooling down",
        ACTIVE = "You can rest now. You can use the OOC regen if you want",
        RESTING = "You are resting now. the OOC regen is active",
        UNKNOWN = "Your UI does not have the OOC regen XML additions and state can not be determined"
    },

    ICON = {
        COMBAT = ICON.MD_REPORT,
        DEBUFFED = ICON.MD_BUG_REPORT,
        COOLDOWN = ICON.MD_QUERY_BUILDER,
        ACTIVE = ICON.MD_INDETERMINATE_CHECK_BOX,
        RESTING = ICON.FA_BED,
        UNKNOWN = ICON.MD_LIVE_HELP
    },

    UNKNOWN_STATE = "UNKNOWN"
}

function displayCharacterStats()
    local combatState = mq.TLO.Me.CombatState()
    local stateColor = COMBATSTATE_MAP.COLOR[combatState] or COMBATSTATE_MAP.COLOR.DEFAULT
    local backgroundColor = stateColor()

    if state.settings.showStatBackgroundColor then
        backgroundColor[4] = state.settings.statBackgroundColorBlend
    else
        backgroundColor[4] = 0
    end

    uiComponents.OffsetCursorPosX(-4)

    uiComponents.DrawWithBackground(renderCharacterStatistics, backgroundColor, uiComponents.GetAvailableX(), 21, 3123, 0, 2)
    uiComponents.ToolTip(COMBATSTATE_MAP.INFO[combatState or COMBATSTATE_MAP.UNKNOWN_STATE])

    if state.settings.showLineSeparators then ImGui.Separator() end
end

function renderCharacterStatistics()
    x = ImGui.GetContentRegionAvail()

    local classShortName = mq.TLO.Me.Class.ShortName()

    local hasMana = eqDataService:classHasMana(classShortName)
    local hasEndurance = eqDataService:classHasEndurance(classShortName)

    local divisor = (hasEndurance and hasMana) and 3 or 2

    ImGui.Columns(divisor, "playerstats")

    uiComponents.OffsetCursorPosX(-4)

    local hpBarLabel = formatStatString("HP", mq.TLO.Me.CurrentHPs(), mq.TLO.Me.MaxHPs(), mq.TLO.Me.PctHPs())
    uiComponents.DrawInfoBar((mq.TLO.Me.PctHPs() or 0) / 100, hpBarLabel, uiComponents.GetAvailableX() + 4, 16)
    uiComponents.ToolTip(hpBarLabel)

    if hasMana then
        uiComponents.NextColumn(state)
        uiComponents.OffsetCursorPosX(-3)
        local mpBarLabel = formatStatString("MP", mq.TLO.Me.CurrentMana(), mq.TLO.Me.MaxMana(), mq.TLO.Me.PctMana())
        uiComponents.DrawInfoBar((mq.TLO.Me.PctMana() or 0) / 100, mpBarLabel, uiComponents.GetAvailableX() + 4, 16, {0, 0, 1, 0.75})
        uiComponents.ToolTip(mpBarLabel)
    end

    if hasEndurance then
        uiComponents.NextColumn(state)
        uiComponents.OffsetCursorPosX(-3)
        local epBarLabel = formatStatString("EP", mq.TLO.Me.CurrentEndurance(), mq.TLO.Me.MaxEndurance(), mq.TLO.Me.PctEndurance())
        uiComponents.DrawInfoBar((mq.TLO.Me.PctEndurance() or 0) / 100, epBarLabel, uiComponents.GetAvailableX() + 4, 16, state.settings.enduranceBarColor)
        uiComponents.ToolTip(epBarLabel)
    end

    ImGui.Columns(1)
end

function formatStatString(label, currentValue, maxValue, percentage)
    currentValue = utils.formatStatValue(currentValue)
    maxValue = utils.formatStatValue(maxValue)

    local label = state.settings.showCharacterPrimaryStatsLabel and (label or "").." " or ""
    local values = state.settings.showCharacterPrimaryStatValues and string.format("[%s / %s] ", currentValue, maxValue) or ""
    local percent = state.settings.showCharacterPrimaryStatPercentage and string.format("%s%% ", percentage) or ""

    return label..values..percent
end

function displayTimeInfo()
    local displayBothTimes = state.settings.showGameTime and state.settings.showEarthTime

    ImGui.BeginGroup()

    if displayBothTimes then ImGui.Columns(2, "timeinfo") end

    if state.settings.showGameTime then
        local hour = mq.TLO.GameTime.Hour()
        if hour >= 19 or hour < 6 then
            ImGui.TextColored(0, 1, 1, 1, ICON.FA_MOON_O)
        else
            ImGui.TextColored(1, 0.85, 0, 1, ICON.MD_WB_SUNNY)
        end

        local timeDisplay = string.format("Norrath Time: %02d:%02d", mq.TLO.GameTime.Hour(), mq.TLO.GameTime.Minute())
        ImGui.SameLine()
        ImGui.Text(timeDisplay)
    end

    if displayBothTimes then
        ImGui.NextColumn()
    end

    if state.settings.showEarthTime then
        ImGui.TextColored(0, 1, 1, 1, ICON.FA_GLOBE)
        ImGui.SameLine()
        local timeDisplay = os.date("Earth Time: %H:%M:%S")
        ImGui.Text(timeDisplay)
    end

    if displayBothTimes then ImGui.Columns(1) end

    ImGui.EndGroup()

    if state.settings.showLineSeparators then ImGui.Separator() end
end

function displayClipboardInfo()
    local clipboardText = ImGui.GetClipboardText() or ""
    if string.len(clipboardText) > 0 then
        if string.len(clipboardText) > 100 then
            clipboardText = string.sub(clipboardText, 1, 100).."..."
        end
        if string.match(clipboardText, "\n") then
            clipboardText = string.sub(clipboardText, 1, string.find(clipboardText, "\n") - 1).."..."
        end
        ImGui.PushTextWrapPos(ImGui.GetFontSize() * 35.0)
        ImGui.Text(ICON.FA_CLIPBOARD.." Clipboard: "..clipboardText)
        ImGui.PopTextWrapPos()
        if state.settings.showLineSeparators then ImGui.Separator() end
    end
end

function displayLastTellInfo()
    local lastTell = mq.TLO.MacroQuest.LastTell()
    if lastTell ~= nil then
        ImGui.Text(ICON.MD_MESSAGE.." Last message from: "..lastTell)
        if state.settings.showLineSeparators then ImGui.Separator() end
    end
end

function displayMacroInfo(macroAccessor, pauseCommand, resumeCommand, endCommand)
    pauseCommand = pauseCommand or defaults.pauseMacro
    resumeCommand = resumeCommand or defaults.resumeMacro
    endCommand = endCommand or defaults.endMacro
    macroAccessor = macroAccessor or defaults.macroAccessor
    if nil ~= macroAccessor().Name() then
        local macro = macroAccessor().Name()
        local paused = macroAccessor().Paused()
        if paused then
            ImGui.TextColored(1, 1, 1, 0.5, ICON.FA_FILE.." Macro: "..macro.." [PAUSED]")
            uiComponents.ToolTip(string.format("The macro file '%s' is currently loaded and paused.", macro))
            if state.settings.showMacroControls then
                ImGui.SameLine()
                local buttonLabel = string.format("%s%s", ICON.MD_PLAY_CIRCLE_OUTLINE, state.settings.showMacroButtonLabels and " Resume" or "")
                if state.settings.showColoredMacroButtons then
                    if uiComponents.SmallButtonColored(buttonLabel, COLOR.GREEN, COLOR.SEAGREEN, COLOR.DARKGREEN) then resumeCommand() end
                else
                    if ImGui.SmallButton(buttonLabel) then resumeCommand() end
                end
                uiComponents.ToolTip("Resume the running macro.")
            end
        else
            ImGui.BeginGroup()
            ImGui.Text(ICON.FA_FILE.." Macro:")
            ImGui.SameLine()
            ImGui.TextColored(0, 1, 0.75, 1, macro)
            ImGui.SameLine()
            ImGui.Text("[Active]")
            ImGui.EndGroup()
            uiComponents.ToolTip(string.format("The macro file '%s' is currently loaded and running.", macro))
            if state.settings.showMacroControls then
                ImGui.SameLine()
                local buttonLabel = string.format("%s%s", ICON.MD_PAUSE_CIRCLE_OUTLINE, state.settings.showMacroButtonLabels and " Pause" or "")
                if ImGui.SmallButton(buttonLabel) then pauseCommand() end
                uiComponents.ToolTip("Pause the running macro.")
            end
        end
        if state.settings.showMacroControls then
            ImGui.SameLine()
            local buttonLabel = string.format("%s%s", ICON.FA_STOP_CIRCLE, state.settings.showMacroButtonLabels and " Stop" or "")
            if state.settings.showColoredMacroButtons then
                if uiComponents.SmallButtonColored(buttonLabel, COLOR.SADDLEBROWN, COLOR.ORANGERED, COLOR.RED) then endCommand() end
            else
                if ImGui.SmallButton(buttonLabel) then endCommand() end
            end
            uiComponents.ToolTip("End the running macro.")
        end
        if state.settings.showLineSeparators then ImGui.Separator() end
    elseif state.settings.showMacroInfoWhenNotActive then
        ImGui.Text(ICON.FA_FILE_O.." No macro loaded")
        uiComponents.ToolTip("There is no macro file currently loaded.")
        if state.settings.showLineSeparators then ImGui.Separator() end
    end
end

function getSpawnDisplayInfo(spawnAccessor)
    local targetName = spawnAccessor().DisplayName()

    if not targetName then
        return nil
    end

    if(spawnAccessor().ID() ~= nil and spawnAccessor().Level() ~= nil) then
        return targetName..' (lvl '..spawnAccessor().Level()..' '..spawnAccessor().Race()..' '..getClassName(spawnAccessor().Class)..' '..ICON.FA_ARROWS_H..string.format(" %.2fm", spawnAccessor().Distance3D())..') '..spawnAccessor().PctHPs()..'%'
    end
end

function getClassName(classReference)
    local useShortName = state.settings.useClassShortNames
    local className = classReference[useShortName and "ShortName" or "Name"]
    if type(className) == "function" or type(className) == "userdata" then
        className = className()
    end
    className = className or "Dinosaur"
    return (className == "UNKNOWN CLASS") and "UNK" or className
end

function displayEntityInfo(spawnAccessor, percentageAccesor, icon, label, defaultMessage)
    ImGui.AlignTextToFramePadding()
    local spawnDisplayInfo = getSpawnDisplayInfo(spawnAccessor)

    if state.settings.showConsiderColor then
        local considerColor = tostring(spawnAccessor().ConColor()):lower()
        local cleanConsiderColor = considerColor:gsub(" ", ""):upper()

        local windowBackgroundColor = state.settings.backgroundColor
        windowBackgroundColor[4] = 0.5

        local conBackgroundColor = COLOR[cleanConsiderColor]
        if nil == conBackgroundColor then conBackgroundColor = {0,0,0,0} end

        uiComponents.DrawWithBackground(function()
            uiComponents.TextColored(icon, uiComponents.getContrastingColor(cleanConsiderColor))
        end, conBackgroundColor, 17, 18, utils.buildIntHash(label), 2, 1)
        if nil ~= spawnAccessor().ConColor() then
            uiComponents.ToolTip("This target cons "..considerColor)
        end
    else
        ImGui.Text(icon)
    end

    ImGui.SameLine()
    ImGui.Text(label)

    local elementOffset = 100
    local yPosition = ImGui.GetCursorPosY()

    if state.settings.showLosIndicator then
        if spawnAccessor().LineOfSight() then
            ImGui.SameLine(elementOffset)
            ImGui.Text(ICON.MD_RECORD_VOICE_OVER)
            uiComponents.ToolTip(ICON.MD_RECORD_VOICE_OVER.." The target is in line of sight")
        end
        elementOffset = elementOffset + 17.5
    end

    if state.settings.showAggroIndicator then
        if spawnAccessor().Aggressive() then
            ImGui.SameLine(elementOffset)
            uiComponents.TextColored(ICON.MD_PRIORITY_HIGH, COLOR.ORANGERED)
            uiComponents.ToolTip(ICON.MD_PRIORITY_HIGH.." The target is currently aggro")
        end
        elementOffset = elementOffset + 17.5
    end

    ImGui.SetCursorPosY(yPosition)

    ImGui.SameLine(elementOffset)
    uiComponents.DrawInfoBar((percentageAccesor() or 0) / 100, spawnDisplayInfo or defaultMessage or "")
end

function displayMATargetInfo()
    local spawnAccessor = defaults.maTargetAccessor
    local percentageAccesor = spawnAccessor().PctHPs
    ImGui.BeginGroup()
    displayEntityInfo(spawnAccessor, percentageAccesor, ICON.FA_CROSSHAIRS, "MA Target:", "No MA Target")
    ImGui.EndGroup()
    local spawnId = spawnAccessor().ID()
    if nil ~= spawnId and spawnId > 0 then
        uiComponents.ToolTip("Click to target.\nClick right mouse button to navigate to target")
        if ImGui.IsItemClicked(ImGuiMouseButton.Right) then
            navService:NavigateToId(spawnAccessor().ID())
        elseif ImGui.IsItemClicked() then
            playerActionsService:TargetById(spawnAccessor().ID())
        end
    end
end

function displayTargetInfo()
    local spawnAccessor = defaults.targetAccessor
    local percentageAccesor = spawnAccessor().PctHPs
    ImGui.BeginGroup()
    displayEntityInfo(spawnAccessor, percentageAccesor, ICON.FA_DOT_CIRCLE_O, "Target:", "No Target")
    ImGui.EndGroup()
    local spawnId = spawnAccessor().ID()
    if nil ~= spawnId and spawnId > 0 then
        uiComponents.ToolTip("Click right mouse button to navigate to target")
        if ImGui.IsItemClicked(ImGuiMouseButton.Right) then
            navService:NavigateToId(spawnAccessor().ID())
        end
    end
end

function displayGMInfo(spawnAccessor)
    spawnAccessor = spawnAccessor or defaults.gmSpawnAccessor
    if spawnAccessor().ID() > 0 then
        local spawnDisplayInfo = getSpawnDisplayInfo(spawnAccessor)
        uiComponents.DrawInfoBar(100, ICON.MD_WARNING.." "..ICON.MD_ERROR.." GM: "..(spawnDisplayInfo or defaultMessage or ""), -1, 20, {state.settings.gmBannerColor[1], state.settings.gmBannerColor[2], state.settings.gmBannerColor[3], 1})
    end
end

function displayZoneInfo()
    local continuation = false

    if state.settings.showZonePlayerCount then
        ImGui.BeginGroup()
        local playerCount = string.format("%s %s", mq.TLO.SpawnCount("PC")(), ICON.MD_PERSON)
        ImGui.TextColored(0,1,0,1,playerCount)
        ImGui.SameLine();
        ImGui.SmallButton(state.breakdownOpen and ICON.FA_SEARCH_MINUS or ICON.FA_SEARCH_PLUS)
        ImGui.EndGroup()

        if state.settings.showZoneBreakdown then
            if ImGui.IsItemHovered() then
                ImGui.BeginTooltip()
                ImGui.PushTextWrapPos(ImGui.GetFontSize() * 35.0)
                ImGui.Text(string.format("Click to %s the breakout window.", state.breakdownOpen and "close" or "open"))
                ImGui.Separator()
                displayZoneDistributionInfo()
                ImGui.PopTextWrapPos()
                ImGui.EndTooltip()
            end

            if ImGui.IsItemClicked() then
                state.breakdownOpen = not state.breakdownOpen
            end
        end
        continuation = true
    end

    if state.settings.showZoneFullName then
        if continuation then
            ImGui.SameLine()
            ImGui.Text("/")
            ImGui.SameLine()
        end
        ImGui.Text(mq.TLO.Zone.Name())
        continuation = true
    end

    if state.settings.showZoneShortName then
        if continuation then
            ImGui.SameLine()
            ImGui.Text("/")
            ImGui.SameLine()
        end
        ImGui.Text(mq.TLO.Zone.ShortName() or "Purgatory")
        continuation = true
    end

    if state.settings.showZoneId then
        if continuation then
            ImGui.SameLine()
            ImGui.Text("/")
            ImGui.SameLine()
        end
        ImGui.Text("id: "..(mq.TLO.Zone.ID() or "8675309"))
    end

    if state.settings.showLineSeparators then ImGui.Separator() end
end

function displayZoneDistributionInfo()
    ImGui.TextColored(0, 1, 0, 1, string.format("%s %s Players", mq.TLO.SpawnCount("PC")(), ICON.MD_PERSON))
    ImGui.SameLine()
    ImGui.Text(string.format("in %s", mq.TLO.Zone.Name()))

    local zoneGuildsBreakdown = zoneDataService:GetGuilds()
    if(nil ~= zoneGuildsBreakdown) then
        ImGui.Separator()

        linq.from(zoneGuildsBreakdown)
            :foreach(function(index, record)
                ImGui.Text(string.format("%s x", record.count))
                ImGui.SameLine()
                local guildText = string.format("<%s>", record.guild)
                if record.guild == mq.TLO.Me.Guild() then
                    uiComponents.TextColored(guildText, {0, 1, 0, 1})
                else
                    ImGui.Text(guildText)
                end
            end)
    end
end

function displayLocationInfo(characterAccessor, locationClickHandler, bookmarkClickHandler, navWaypointClickHandler)
    locationClickHandler = locationClickHandler or function(coordinates) ImGui.SetClipboardText(coordinates) end

    characterAccessor = characterAccessor or defaults.meAccessor
    local coords = string.format("%.2f", characterAccessor().Y()).." "..string.format("%.2f", characterAccessor().X()).." "..string.format("%.2f", characterAccessor().Z())

    if state.settings.showLocation then
        if ImGui.SmallButton(ICON.FA_LOCATION_ARROW.." "..coords:gsub('% ', " / ")) then
            locationClickHandler(coords)
        end
        uiComponents.ToolTip("Your current location.\nClick to copy to clipboard.")
    end

    if state.settings.showLocationBookmark then
        displayLocationBookmarker(coords, bookmarkClickHandler)
    end

    if state.settings.showLocationRecordedWaypoints then
        displayWaypointNavigator(navWaypointClickHandler)
    end
end

function displayCharacterInfo(characterAccessor, locationClickHandler, bookmarkClickHandler, navWaypointClickHandler)
    locationClickHandler = locationClickHandler or function(coordinates) ImGui.SetClipboardText(coordinates) end

    characterAccessor = characterAccessor or defaults.meAccessor
    ImGui.BeginGroup()
    displayInvisInfo(characterAccessor)

    if state.settings.showCharacterLevelAndClass then
        ImGui.Text((characterAccessor().Level() or "625").." "..getClassName(characterAccessor().Class))
    end
    ImGui.EndGroup()
    if ImGui.IsItemClicked() then
        playerActionsService:TargetMyself()
    end
    uiComponents.ToolTip("Click to target yourself")

    if state.settings.showSubscriptionStatus then
        local subscription = characterAccessor().Subscription()
        local isGold = (subscription == "GOLD")
        local subscriptionIcon = isGold and ICON.FA_CHECK_CIRCLE or ICON.FA_MINUS_CIRCLE
        local subscriptionColor = isGold and {1.0, 0.709, 0.0, 1.0} or {0.781, 0.779, 0.774, 1.0}
        ImGui.SameLine()
        uiComponents.TextColored(string.format("%s%s", subscriptionIcon, state.settings.showSubscriptionStatusText and " "..subscription or ""), subscriptionColor)
        uiComponents.ToolTip("Your current subcription status is "..subscription)
    end

    if state.settings.showCombatStateIcon then
        local combatState = mq.TLO.Me.CombatState()
        local stateColor = COMBATSTATE_MAP.COLOR[combatState] or COMBATSTATE_MAP.COLOR.DEFAULT
        local stateIcon = COMBATSTATE_MAP.ICON[combatState] or COMBATSTATE_MAP.ICON.UNKNOWN

        ImGui.SameLine()
        uiComponents.DrawWithBackground(stateIcon, stateColor(), 18, 18, 2152, 1, 1)
        uiComponents.ToolTip(COMBATSTATE_MAP.INFO[combatState] or COMBATSTATE_MAP.INFO.UNKNOWN)
    end

    if state.settings.showLevitatingStatus then
        if characterAccessor().Levitating() then
            ImGui.SameLine()
            ImGui.BeginGroup()
            ImGui.Text(ICON.MD_PUBLISH)
            if state.settings.showLevitatingStatusText then
                ImGui.SameLine()
                ImGui.Text("LEV")
            end
            ImGui.EndGroup()
            if ImGui.IsItemClicked() then
                playerActionsService.RemoveLevitationEffects()
            end
            uiComponents.ToolTip("You are levitating.\nClick to remove levitation effects.")
        end
    end

    if state.settings.showMovementStatus then
        if mq.TLO.Navigation.Active() then
            ImGui.SameLine()
            ImGui.BeginGroup()
            ImGui.Text(ICON.MD_DIRECTIONS_RUN)
            if state.settings.showMovementStatusText then
                ImGui.SameLine()
                ImGui.Text("Speed:")
                ImGui.SameLine()
                ImGui.Text(mq.TLO.Navigation.Velocity())
            end
            ImGui.EndGroup()
            uiComponents.ToolTip("You are currently navigating.")
        end
    end

    if state.settings.groupCharacterAndLocation then
        ImGui.SameLine()
        displayLocationInfo(characterAccessor, locationClickHandler, bookmarkClickHandler, navWaypointClickHandler)
    end

    if state.settings.showLineSeparators then ImGui.Separator() end
end

function displayInvisInfo(characterAccessor)
    if characterAccessor().Invis() and state.settings.showInvisStatus then
        ImGui.BeginGroup()

        local normalInvis = characterAccessor().Invis("normal")
        normalInvis = normalInvis and normalInvis()
        local undeadInvis = characterAccessor().Invis("undead")
        undeadInvis = undeadInvis and undeadInvis()
        local invisSymbols = ((normalInvis and ICON.FA_EYE) or '')..((undeadInvis and ICON.MD_REMOVE_RED_EYE) or '')

        uiComponents.TextColored(string.format("(%s %sINVIS)", invisSymbols, (normalInvis and undeadInvis) and "2x" or ""), state.settings.invisStatusColor)

        ImGui.EndGroup()
        uiComponents.ToolTip(function()
            local ivuMessage = "You are "..ICON.MD_REMOVE_RED_EYE.." invisible vs undead."
            local invisMessage = "You are "..ICON.FA_EYE.." invisible."
            uiComponents.TextColored(string.format("%s%s%s", normalInvis and invisMessage or '', (normalInvis and undeadInvis) and "\n" or '', undeadInvis and ivuMessage or ''), state.settings.invisStatusColor)
        end)
        ImGui.SameLine()
    end
end

function displayWaypointNavigator(navWaypointClickHandler)
    navWaypointClickHandler = navWaypointClickHandler or function()
        ImGui.OpenPopup("WaypointNavigator")
    end

    ImGui.SameLine()
    if ImGui.SmallButton(ICON.MD_MY_LOCATION.." "..ICON.FA_CARET_DOWN) then
        navWaypointClickHandler()
    end

    if ImGui.BeginPopup("WaypointNavigator", ImGuiWindowFlags.NoMove + ImGuiWindowFlags.NoResize) then
        local waypoints = navService:GetWaypointsForZone(mq.TLO.Zone.ShortName())

        if nil == waypoints or nil == next(waypoints) then
            ImGui.TextColored(0.15, 0.65, 0.85, 1, ICON.MD_INFO)
            ImGui.SameLine()
            ImGui.Text("No saved waypoints available")
        else
            ImGui.Text(string.format("%s Select Waypoint", ICON.MD_MY_LOCATION))
            ImGui.Separator()

            linq.from(waypoints)
                :orderBy(function(location, waypoint) return waypoint end)
                :foreach(function(waypoint, location)
                    if ImGui.Button(waypoint, 200, 20) then
                        -- I was running into an issue where nav save/load wasn't providing the ability to nav
                        -- to newly saved waypoints from another character until the plugin was reloaded. This
                        -- is using the coords stored for the waypoint in the ini to travel instead of the wp name
                        navService:NavigateToWaypointLoc(location)
                        ImGui.CloseCurrentPopup()
                    end
                end)
        end

        ImGui.EndPopup()
    else
        uiComponents.ToolTip("Navigate to a saved MQ2Nav waypoint.")
    end
end

function displayLocationBookmarker(coords, bookmarkClickHandler)
    bookmarkClickHandler = bookmarkClickHandler or function()
        ImGui.OpenPopup("LocationBookmark")
    end

    ImGui.SameLine()
    if ImGui.SmallButton(ICON.FA_BOOKMARK) then
        bookmarkClickHandler()
    end

    if ImGui.BeginPopup("LocationBookmark", ImGuiWindowFlags.NoMove + ImGuiWindowFlags.NoResize) then
        ImGui.Text(string.format("%s Record Waypoint: %s", ICON.FA_BOOKMARK, coords))
        ImGui.Separator()
        inputState.bookmarkText = ImGui.InputText("Waypoint Name", inputState.bookmarkText, ImGuiInputTextFlags.EnterReturnsTrue)
        ImGui.Separator()
        if ImGui.Button("Cancel") then
            inputState.bookmarkText = ""
            ImGui.CloseCurrentPopup()
        end
        ImGui.SameLine()
        if ImGui.Button("Save") then
            navService:RecordWaypoint(inputState.bookmarkText, coords)
            inputState.bookmarkText = ""
            ImGui.CloseCurrentPopup()
        end

        ImGui.EndPopup()
    else
        uiComponents.ToolTip("Save current position as a waypoint in MQ2Nav.")
    end
end

local uiRenderers = {
    displayGMInfo,
    displayCharacterInfo,
    displayCharacterStats,
    displayExperienceInfo,
    displayZoneInfo,
    displayLastTellInfo,
    displayMacroInfo,
    displayTimeInfo,
    displayMonitoredTargetInfo,
    displayClipboardInfo,
    displayCustomElements
}

return {
    displayGMInfo = displayGMInfo,
    displayCharacterInfo = displayCharacterInfo,
    displayCharacterStats = displayCharacterStats,
    displayExperienceInfo = displayExperienceInfo,
    displayZoneInfo = displayZoneInfo,
    displayLastTellInfo = displayLastTellInfo,
    displayMacroInfo = displayMacroInfo,
    displayTimeInfo = displayTimeInfo,
    displayMonitoredTargetInfo = displayMonitoredTargetInfo,
    displayClipboardInfo = displayClipboardInfo,
    displayEntityInfo = displayEntityInfo,
    COMBATSTATE_MAP = COMBATSTATE_MAP,
    uiRenderers = uiRenderers
}
