local state = {}

local function getState(id, defaultValues)
    local context = state[id]
    if not context then
        print("generating new state for " .. id)
        context = defaultValues
        state[id] = context
    end
    return context
end

local function setState(id, value)
    local context = getState(id)
    if context then
        for k, v in pairs(context) do
            context[k] = value[k]
        end
    end
    state[id] = value
end

return {
    getState = getState,
    setState = setState
}
