local TEXT_BASE_WIDTH = ImGui.CalcTextSize("A")
local TEXT_BASE_HEIGHT = ImGui.GetTextLineHeightWithSpacing()
local animSpellIcons = mq.FindTextureAnimation('A_SpellIcons')
local iconSize = 24
local hasBuffs = false

ui.StartWrapPanel(iconSize)

for index = 1, mq.TLO.Me.MaxBuffSlots() do
  local spell = mq.TLO.Me.Buff(index)
  if spell() then
    ui.NextWrapPanelItem()
    hasBuffs = true

    local xPos = ImGui.GetCursorPosX()
    local yPos = ImGui.GetCursorPosY()

    ImGui.BeginGroup()

    animSpellIcons:SetTextureCell(spell.SpellIcon())
    ImGui.DrawTextureAnimation(animSpellIcons, iconSize, iconSize)

    ImGui.SameLine()

    local durationLabel = spell.Duration.TotalSeconds() .. "s"
    if spell.Duration.TotalMinutes() > 0 then
        durationLabel = spell.Duration.TotalMinutes() .. "m"
    end
    if spell.Duration.TotalMinutes() > 60 then
        if spell.Duration.TotalMinutes() / 60 > 100 then
            durationLabel = "Permanent"
        else
            durationLabel = string.format("%d", spell.Duration.TotalMinutes() / 60) .. "h"
        end
    end

    local height = ImGui.GetFontSize()

    if durationLabel ~= "Permanent" then
        ImGui.SetCursorPosX(xPos)

        ImGui.SetCursorPosY(yPos + iconSize - height)

        ui.renderWithBackground(durationLabel, {
            width = iconSize,
            height = height,
            backgroundColor = {0, 0, 0, 0.15},
            paddingY = 1
        })
    end

    ImGui.EndGroup()

    ui.ToolTip(function()
        animSpellIcons:SetTextureCell(spell.SpellIcon())
        ImGui.DrawTextureAnimation(animSpellIcons, iconSize, iconSize)
        ImGui.SameLine()
        ImGui.Text(string.format("%s (lvl %s)", spell.Name(), spell.Level()))
        -- if spell.CastTime() and spell.MyCastTime() then
        --     ImGui.Text(string.format("Cast Time: %s (%s)" spell.CastTime(), spell.MyCastTime()))
        -- end
        -- ImGui.Text(string.format("Cast Time: %s (%s)" spell.CastTime(), spell.MyCastTime()))
        if spell.CounterType() and spell.CounterType() ~= "None" then
            ImGui.Text("Counter: " .. spell.CounterType())
        end

        ImGui.Text("Remaining Duration: " .. ((durationLabel == "Permanent") and durationLabel or spell.Duration.TimeHMS()))
    end)

    ImGui.SetCursorPosY(yPos + iconSize + 2)
  end
end

ui.EndWrapPanel()

if not hasBuffs then
    ImGui.Text("<no buffs>")
end
