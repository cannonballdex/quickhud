--- @type Mq
local mq = require('mq')

--- @type ImGui
require 'ImGui'

-- Logging
local log = require('quickhud.lib.Write')

-- Libraries
local ICON = require('quickhud.lib.icons')
local utils = require('quickhud.utils')
local uiComponents = require('quickhud.uihelpers')
local services = require('quickhud.services')
local state = require('quickhud.state')
local ui = require('quickhud.quickhudui')
local linq = require("quickhud.lib.lazylualinq.linq")

-- Services
local mockDataService = services.MockData
local appSettingsService = services.AppSettings

local EMPTY_DROPDOWN_SELECTION_TEXT = "<none>"

local THEMES = {
    { label = "Default", theme = { textColor = { utils.hex("#ffffff", 1) }, backgroundColor = { utils.hex("#242424", 1) } } },
    { label = "Royal", theme = { textColor = { utils.hex("#fcc729", 1) }, backgroundColor = { utils.hex("#337def", 1) } } },
    { label = "Vibrant", theme = { textColor = { utils.hex("#f7db00", 1) }, backgroundColor = { utils.hex("#4f0073", 1) } } },
    { label = "Vice City", theme = { textColor = { utils.hex("#00FFFF", 1) }, backgroundColor = { utils.hex("#FF69B4", 1) } } },
    { label = "Two Blue", theme = { textColor = { utils.hex("#ADD8E6", 1) }, backgroundColor = { utils.hex("#00008b", 1) } } },
    { label = "Bubblegum", theme = { textColor = { utils.hex("#89ABE3FF", 1) }, backgroundColor = { utils.hex("#EA738DFF", 1) } } },
    { label = "Burnt Orange", theme = { textColor = { utils.hex("#FCEDDA", 1) }, backgroundColor = { utils.hex("#EE4E34", 1) } } },
    { label = "Retro", theme = { textColor = { utils.hex("#00ff00", 1) }, backgroundColor = { utils.hex("#000000", 1) } } },
    { label = "Regal", theme = { textColor = { utils.hex("#f4b41a", 1) }, backgroundColor = { utils.hex("#143d59", 1) } } },
    { label = "Black n' yellow", theme = { textColor = { utils.hex("#FEE715", 1) }, backgroundColor = { utils.hex("#101820", 1) } } },
    { label = "Purple", theme = { textColor = { utils.hex("#b8df10", 1) }, backgroundColor = { utils.hex("#390879", 1) } } }
}

local THEME_NAMES = linq.from(THEMES):select(function(element) return element.label end):toArray()

local inputState = {
    copySettingsSelection = EMPTY_DROPDOWN_SELECTION_TEXT,
    selectedTheme = EMPTY_DROPDOWN_SELECTION_TEXT,
    appliedTheme = EMPTY_DROPDOWN_SELECTION_TEXT
}

local COMBATSTATE_TOOLTIP = [[
States
    Combat: ]]..ui.COMBATSTATE_MAP.INFO.COMBAT..[[

    Debuffed: ]]..ui.COMBATSTATE_MAP.INFO.DEBUFFED..[[

    Cooldown: ]]..ui.COMBATSTATE_MAP.INFO.COOLDOWN..[[

    Active: ]]..ui.COMBATSTATE_MAP.INFO.ACTIVE..[[

    Resting: ]]..ui.COMBATSTATE_MAP.INFO.RESTING..[[
]]

function renderAdvancedSettings(state)
    -- Configuration Options
    ImGui.TextDisabled("Configuration Options")
    ImGui.Separator()
    if ImGui.RadioButton("Use Shared Settings", not state.settings.useIndividualSettings) and state.settings.useIndividualSettings then
        state.settings = appSettingsService:UseSharedSettings(state.settings)
    end
    uiComponents.ToolTip("This will share settings across characters that have this option selected.\n\nThis option is best if you would like your HUD to be uniform across characters.")
    ImGui.SameLine()
    if ImGui.RadioButton("Use Character Specific Settings", state.settings.useIndividualSettings) and not state.settings.useIndividualSettings then
        state.settings = appSettingsService:UseIndividualSettings(state.settings)
    end
    uiComponents.ToolTip("This will allow your character to have indivdual settings that are not shared by other characters.\n\nThis option is best if you would like each character to have a unique HUD configuration.")

    ImGui.Text("Copy Existing Configuration")
    inputState.copySettingsSelection = uiComponents.DrawComboBox("##copyExistingConfig",
        inputState.copySettingsSelection,
        utils.tableConcat({EMPTY_DROPDOWN_SELECTION_TEXT}, appSettingsService:EnumerateExistingConfigurations())
    )

    ImGui.SameLine()
    local hasFileSelection = (string.len(inputState.copySettingsSelection) > 0) and inputState.copySettingsSelection ~= EMPTY_DROPDOWN_SELECTION_TEXT
    if hasFileSelection then
        if ImGui.Button("Copy") then
            log.Debug(string.format("copy file %s", inputState.copySettingsSelection))
            state.settings = appSettingsService:CopySettings(inputState.copySettingsSelection)
        end
        uiComponents.ToolTip(string.format("Copy configuration of %s", inputState.copySettingsSelection))
    else
        uiComponents.DisabledButton("Copy")
        uiComponents.ToolTip("Choose a file to copy an existing configuration")
    end

    local loadedConfigFileName = (state.settings.useIndividualSettings and appSettingsService:GetIndividualSettingsFileName() or appSettingsService.SharedSettingsFileName):gsub('/', '\\')
    local loadedConfigFilePath = (state.settings.useIndividualSettings and appSettingsService:GetIndividualSettingsFilePath() or appSettingsService:GetSharedSettingsFilePath()):gsub('/', '\\')
    ImGui.Text(string.format("Loaded Config: %s", loadedConfigFileName))
    if ImGui.IsItemClicked() then
        os.execute(string.format('explorer "%s"', loadedConfigFilePath))
    end
    uiComponents.ToolTip('Click to open: '..loadedConfigFilePath)
end

function renderGeneralSettings(state)
    ImGui.TextDisabled("Window Settings")
    ImGui.Separator()

    state.settings.useClassShortNames = uiComponents.DrawCheckBox("Use class short names", "##useClassShortNames", state.settings.useClassShortNames, "This option will have the UI prefer class short names to full names where class is displayed.")
    state.settings.windowOpacity = (ImGui.SliderInt("Window Opacity", state.settings.windowOpacity * 100, 0, 100, "%d %%") / 100)
    state.settings.contentOpacity = (ImGui.SliderInt("Content Opacity", state.settings.contentOpacity * 100, 10, 100, "%d %%") / 100)
    state.settings.paddingMultiplier = ImGui.SliderFloat("Content Spacing", state.settings.paddingMultiplier, 0, 2, "%.2f")
    state.settings.showTitleBar = uiComponents.DrawCheckBox("Show Title Bar", "##showTitleBar", state.settings.showTitleBar, "Toggles on/off the titlebar for the hud window.")
    state.settings.showLineSeparators = uiComponents.DrawCheckBox("Show Line Separators", "##showLineSeparators", state.settings.showLineSeparators, "Displayes horizontal rule line between vertical ui elements")

    state.settings.backgroundColor = uiComponents.DrawColorEditor("Window Background Color", state.settings.backgroundColor)
    state.settings.textColor = uiComponents.DrawColorEditor("Window Text Color", state.settings.textColor)
    if ImGui.SmallButton("Swap Colors") then
        local backgroundColor = state.settings.backgroundColor
        state.settings.backgroundColor = state.settings.textColor
        state.settings.textColor = backgroundColor
    end

    ImGui.Text("Select a theme")
    inputState.selectedTheme = uiComponents.DrawComboBox("##themeSelection",
        inputState.selectedTheme,
        utils.tableConcat({EMPTY_DROPDOWN_SELECTION_TEXT}, THEME_NAMES)
    )

    local hasNewThemeSelection = (string.len(inputState.selectedTheme) > 0) and inputState.selectedTheme ~= EMPTY_DROPDOWN_SELECTION_TEXT and inputState.appliedTheme ~= inputState.selectedTheme
    if hasNewThemeSelection then
        local theme = linq.from(THEMES):first(function(entry) return entry.label == inputState.selectedTheme end).theme
        state.settings.backgroundColor = theme.backgroundColor
        state.settings.textColor = theme.textColor
        inputState.appliedTheme = inputState.selectedTheme
    end
end

function renderGmBarSettings(state)
    -- GM Info Settings
    state.settings.showGMBanner = uiComponents.DrawCheckBox("Show GM Banner", "##showGMBanner", state.settings.showGMBanner, "Show a banner at the top of the HUD if there is a GM in zone.")
    ImGui.Indent()
    ImGui.TextDisabled("Preview")
    ui.displayGMInfo(mockDataService.FakeCharacterAccessor)
    ImGui.Unindent()
    if state.settings.showGMBanner then
        ImGui.Separator()
        ImGui.Indent()
        ImGui.TextDisabled("GM Banner Options")
        state.settings.gmBannerColor = uiComponents.DrawColorEditor("GM Banner Color", state.settings.gmBannerColor)
        ImGui.Unindent()
    end
end

function renderCharacterSettings(state)
    -- Character Info Settings
    state.settings.showCharacterInfo = uiComponents.DrawCheckBox("Show Character Information", "##showCharacterInfo", state.settings.showCharacterInfo)
    ImGui.Indent()
    ImGui.TextDisabled("Preview")
    ui.displayCharacterInfo(mockDataService.FakeCharacterAccessor, utils.noop, utils.noop, utils.noop)
    ImGui.Unindent()
    if state.settings.showCharacterInfo then
        ImGui.Indent()
        ImGui.TextDisabled("Character Data Options")

        state.settings.showCharacterIcon = uiComponents.DrawCheckBox("Show Character Icon", "##showCharacterIcon", state.settings.showCharacterIcon, "Displays the "..state.settings.characterIcon.." icon in the character row.")
        if state.settings.showCharacterIcon then
            ImGui.Indent()
            state.settings.characterIcon = uiComponents.DrawIconChooser(state.settings.characterIcon, "Character Icon")
            ImGui.Unindent()
        end
        state.settings.showInvisStatus = uiComponents.DrawCheckBox("Show Invis Status", "##showInvisStatus", state.settings.showInvisStatus,
            function ()
                ImGui.Text("Show an indicator ")
                ImGui.SameLine()
                ImGui.TextColored(state.settings.invisStatusColor[1], state.settings.invisStatusColor[2], state.settings.invisStatusColor[3], 1, "("..ICON.MD_REMOVE_RED_EYE.." INVIS)")
                ImGui.SameLine()
                ImGui.Text("in the character line when invisible.")
            end
        )
        if state.settings.showInvisStatus then
            ImGui.Indent()
            state.settings.invisStatusColor = uiComponents.DrawColorEditor("Invis Indicator Color", state.settings.invisStatusColor)
            ImGui.Unindent()
        end

        state.settings.showCharacterLevelAndClass = uiComponents.DrawCheckBox("Show Character Level & Class", "##showCharacterLevelAndClass", state.settings.showCharacterLevelAndClass)

        state.settings.showSubscriptionStatus = uiComponents.DrawCheckBox("Show Subscription Status", "##showSubscriptionStatus", state.settings.showSubscriptionStatus)
        if state.settings.showSubscriptionStatus then
            ImGui.Indent()
            state.settings.showSubscriptionStatusText = uiComponents.DrawCheckBox("Show Subscription Status Text", "##showSubscriptionStatusText", state.settings.showSubscriptionStatusText)
            ImGui.Unindent()
        end

        state.settings.showCombatStateIcon = uiComponents.DrawCheckBox("Show combat state icon", "##showCombatStateIcon", state.settings.showCombatStateIcon, "Displays an icon that displays the combat state of your character. This utilizes the background colors defined in the Stat Bars section when that option is enabled.")

        state.settings.showLocation = uiComponents.DrawCheckBox("Show Location", "##showLocation", state.settings.showLocation)
        state.settings.showLocationBookmark = uiComponents.DrawCheckBox("Show Location Bookmark Button", "##showLocationBookmark", state.settings.showLocationBookmark, "Shows a button in the HUD that allows you to record your current location as a waypoint with MQ2Nav")
        state.settings.showLocationRecordedWaypoints = uiComponents.DrawCheckBox("Show Recorded Waypoints Menu", "##showLocationRecordedWaypoints", state.settings.showLocationRecordedWaypoints, "Displays a list of waypoints available for this zone and allows you to click to navigate.\nThis list is populated by parsing the MQ2Nav ini file.")

        state.settings.showLevitatingStatus = uiComponents.DrawCheckBox("Show Levitating Status", "##showLevitatingStatus", state.settings.showLevitatingStatus, "Will show a levitating indicator if you are currently levitating.")
        if state.settings.showLevitatingStatus then
            ImGui.Indent()
            state.settings.showLevitatingStatusText = uiComponents.DrawCheckBox("Show Levitating Status Label", "##showLevitatingStatusText", state.settings.showLevitatingStatusText, "Shows 'LEV' next to the levitating indicator.")
            ImGui.Unindent()
        end
        state.settings.showMovementStatus = uiComponents.DrawCheckBox("Show Movement Status", "##showMovementStatus", state.settings.showMovementStatus, "If you have a naviation path active this option will show a navigation status indicator.")
        if state.settings.showMovementStatus then
            ImGui.Indent()
            state.settings.showMovementStatusText = uiComponents.DrawCheckBox("Show Movement Status Text", "##showMovementStatusText", state.settings.showMovementStatusText, "Shows 'Speed' with the current character velocity next to the navigation indicator.")
            ImGui.Unindent()
        end
        ImGui.Unindent()
    end
end

function renderStatBars(state)
    -- Primary Stat Info Settings
    state.settings.showCharacterPrimaryStats = uiComponents.DrawCheckBox("Show Character Stat Bars", "##showCharacterPrimaryStats", state.settings.showCharacterPrimaryStats)
    ImGui.Indent()
    ImGui.TextDisabled("Preview")
    ui.displayCharacterStats()
    ImGui.Unindent()
    if state.settings.showCharacterPrimaryStats then
        ImGui.Indent()
        ImGui.TextDisabled("Character Stat Options")
        state.settings.showCharacterPrimaryStatsLabel = uiComponents.DrawCheckBox("Show Label on Stat Bars", "##showCharacterPrimaryStatsLabel", state.settings.showCharacterPrimaryStatsLabel, "Display the label of the stat on the display bar.")
        state.settings.showCharacterPrimaryStatValues = uiComponents.DrawCheckBox("Show Values on Stat Bars", "##showCharacterPrimaryStatValues", state.settings.showCharacterPrimaryStatValues, "Display the actual values of the stat on the display bar.")
        state.settings.showCharacterPrimaryStatPercentage = uiComponents.DrawCheckBox("Show Percentage on Stat Bars", "##showCharacterPrimaryStatPercentage", state.settings.showCharacterPrimaryStatPercentage, "Display the percentage of the stat on the display bar.")
        state.settings.showStatBackgroundColor = uiComponents.DrawCheckBox("Show Combat State Background Color", "##showStatBackgroundColor", state.settings.showStatBackgroundColor, "Change the background color of the stats area to indicate which combat state you are currently in.\n\n"..COMBATSTATE_TOOLTIP)
        if state.settings.showStatBackgroundColor then
            ImGui.Indent()
            ImGui.Text("Background Alpha Level")
            ImGui.SameLine()
            uiComponents.HelpMarker("Applies an alpha effect to the background color independent of to the window opacity causing the background color to be more or less opaque.")
            state.settings.statBackgroundColorBlend = (ImGui.SliderInt("Alpha amount", state.settings.statBackgroundColorBlend * 100, 0, 100, "%d") / 100)
            state.settings.restingStateBackgroundColor = uiComponents.DrawColorEditor("Resting State Background Color", state.settings.restingStateBackgroundColor)
            state.settings.combatStateBackgroundColor = uiComponents.DrawColorEditor("Combat State Background Color", state.settings.combatStateBackgroundColor)
            state.settings.activeStateBackgroundColor = uiComponents.DrawColorEditor("Active State Background Color", state.settings.activeStateBackgroundColor)
            state.settings.debuffedStateBackgroundColor = uiComponents.DrawColorEditor("Debuffed State Background Color", state.settings.debuffedStateBackgroundColor)
            state.settings.cooldownStateBackgroundColor = uiComponents.DrawColorEditor("Cooldown State Background Color", state.settings.cooldownStateBackgroundColor)
            ImGui.Unindent()
        end
        ImGui.Unindent()
    end
end

function renderExperienceBars(state)
    -- Experience Info Settings
    state.settings.showExperienceInfo = uiComponents.DrawCheckBox("Show Experience Bars", "##showExperienceInfo", state.settings.showExperienceInfo)
    ImGui.Indent()
    ImGui.TextDisabled("Preview")
    ui.displayExperienceInfo()
    ImGui.Unindent()
    if state.settings.showExperienceInfo then
        ImGui.Indent()
        ImGui.TextDisabled("Experience Bar Options")
        state.settings.showExperience = uiComponents.DrawCheckBox("Show Character Experience", "##showExperience", state.settings.showExperience)
        state.settings.showAAExperience = uiComponents.DrawCheckBox("Show Character AA Experience", "##showAAExperience", state.settings.showAAExperience)
        state.settings.showMercAAExperience = uiComponents.DrawCheckBox("Show Merc AA Experience", "##showMercAAExperience", state.settings.showMercAAExperience)
        ImGui.Unindent()
    end
end

function renderZoneSettings(state)
    -- Zone Info Settings
    state.settings.showZoneInfo = uiComponents.DrawCheckBox("Show Zone Information", "##showZoneInfo", state.settings.showZoneInfo)
    ImGui.Indent()
    ImGui.TextDisabled("Preview")
    ui.displayZoneInfo()
    ImGui.Unindent()
    if state.settings.showZoneInfo then
        ImGui.Indent()
        ImGui.TextDisabled("Zone Info Options")
        state.settings.showZonePlayerCount = uiComponents.DrawCheckBox("Show Number of Players in Zone", "##showZonePlayerCount", state.settings.showZonePlayerCount, "Shows the number of players in the current zone")
        if state.settings.showZonePlayerCount then
            ImGui.Indent()
            state.settings.showZoneBreakdown = uiComponents.DrawCheckBox("Show Tooltip with Breakdown of Characters by Guild", "##showZoneBreakdown", state.settings.showZoneBreakdown, "Provide a toolip with a breakdown of the characters in the zone grouped by the guild (unguilded for non-guilded characters) and sorted by the grouped count in descending order.")
            ImGui.Unindent()
        end
        state.settings.showZoneFullName = uiComponents.DrawCheckBox("Show Zone Full Name", "##showZoneFullName", state.settings.showZoneFullName)
        state.settings.showZoneShortName = uiComponents.DrawCheckBox("Show Zone Short Name", "##showZoneShortName", state.settings.showZoneShortName)
        state.settings.showZoneId = uiComponents.DrawCheckBox("Show Zone Id", "##showZoneId", state.settings.showZoneId)
        ImGui.Unindent()
    end
end

function renderMacroSettings(state)
    -- Macro Settings
    state.settings.showMacroInfo = uiComponents.DrawCheckBox("Show Macro Information", "##showMacroInfo", state.settings.showMacroInfo)
    ImGui.Indent()
    ImGui.TextDisabled("Running Preview")
    ui.displayMacroInfo(mockDataService.GenerateMockMacro("kissassist.mac", false), utils.noop, utils.noop, utils.noop)
    ImGui.TextDisabled("Paused Preview")
    ui.displayMacroInfo(mockDataService.GenerateMockMacro("kissassist.mac", true), utils.noop, utils.noop, utils.noop)
    if state.settings.showMacroInfoWhenNotActive then
        ImGui.TextDisabled("No Macro Preview")
        ui.displayMacroInfo(mockDataService.GenerateMockMacro(nil, false), utils.noop, utils.noop, utils.noop)
    end
    ImGui.Unindent()
    if state.settings.showMacroInfo then
        ImGui.Indent()
        ImGui.TextDisabled("Macro Info Options")
        state.settings.showMacroInfoWhenNotActive = uiComponents.DrawCheckBox("Always Show Macro Line", "##showMacroInfoWhenNotActive", state.settings.showMacroInfoWhenNotActive, "This option will always display the macro line otherwise the macro line will be hidden when no macro is active.")
        state.settings.showMacroControls = uiComponents.DrawCheckBox("Show Macro Controls", "##showMacroControls", state.settings.showMacroControls, "Will show the Play/Pause and Stop buttons next to the macro for easy access.")
        state.settings.showMacroButtonLabels = uiComponents.DrawCheckBox("Show Button Labels", "##showMacroButtonLabels", state.settings.showMacroButtonLabels, "Will show the labels on the buttons.")
        state.settings.showColoredMacroButtons = uiComponents.DrawCheckBox("Use Colored Buttons", "##showColoredMacroButtons", state.settings.showColoredMacroButtons, "Will use custom coloring for the resume and stop buttons.")
        ImGui.Unindent()
    end
end

function renderTimeSettings(state)
    -- Time Settings
    state.settings.showTimeInfo = uiComponents.DrawCheckBox("Show Time Information", "##showTimeInfo", state.settings.showTimeInfo)
    ImGui.Indent()
    ImGui.TextDisabled("Preview")
    ui.displayTimeInfo()
    ImGui.Unindent()
    if state.settings.showMonitoredTargetInfo then
        ImGui.Indent()
        ImGui.TextDisabled("Time Info Options")
        state.settings.showGameTime = uiComponents.DrawCheckBox("Show Game Time", "##showGameTime", state.settings.showGameTime)
        state.settings.showEarthTime = uiComponents.DrawCheckBox("Show Earth Time", "##showEarthTime", state.settings.showEarthTime)
        ImGui.Unindent()
    end
end

function renderTargetSettings(state)
    -- Auto Target Settings
    state.settings.showMonitoredTargetInfo = uiComponents.DrawCheckBox("Show Target Information", "##showMonitoredTargetInfo", state.settings.showMonitoredTargetInfo, "Display the Name, Level, Race, Class, Distance and Health Percentage of player and MA targets.")
    ImGui.Indent()
    ImGui.TextDisabled("Preview")
    ui.displayEntityInfo(mockDataService.FakeCharacterAccessor, mockDataService.FakeCharacterAccessor().PctHPs, ICON.FA_DOT_CIRCLE_O, "EX:", "No Target")
    ImGui.Unindent()
    if state.settings.showMonitoredTargetInfo then
        ImGui.Indent()
        ImGui.TextDisabled("Target Info Options")
        state.settings.showAggroIndicator = uiComponents.DrawCheckBox("Show Aggro Indicator", "##showAggroIndicator", state.settings.showAggroIndicator)
        state.settings.showLosIndicator = uiComponents.DrawCheckBox("Show LOS Indicator", "##showLosIndicator", state.settings.showLosIndicator)
        state.settings.showMyTargetInfo = uiComponents.DrawCheckBox("Show My Target", "##showMyTargetInfo", state.settings.showMyTargetInfo)
        state.settings.showMATargetInfo = uiComponents.DrawCheckBox("Show MA Target", "##showMATargetInfo", state.settings.showMATargetInfo)
        state.settings.showConsiderColor = uiComponents.DrawCheckBox("Show Consider Colors", "##showConsiderColor", state.settings.showConsiderColor, "The target line icon will indicate the consider color for each target.")
        ImGui.Unindent()
    end
end

local function renderDisplayOrderSettings(state)
    ImGui.Text("Change Display Order")
    state.settings.uiElements = uiComponents.ReorderableList(
        linq.from(state.settings.uiElements):orderBy(function(element) return element.order end):toArray(),
        function(index, item)
            ImGui.Text(tostring(index)..": "..item.title)
        end,
        function(list, element, oldIndex, newIndex)
            list[oldIndex].order = newIndex
            list[newIndex].order = oldIndex
        end)
end

function renderClipboardSettings(state)
    -- Clipboard Settings
    state.settings.showClipboardInfo = uiComponents.DrawCheckBox("Show Clipboard Information", "##showClipboardInfo", state.settings.showClipboardInfo, "This will display the current text contents of the clipboard")
    ImGui.Indent()
    ImGui.TextDisabled("Preview")
    ui.displayClipboardInfo()
    ImGui.Unindent()
end

table.insert(state.settingsComponents, {title = "General", renderer = renderGeneralSettings})
table.insert(state.settingsComponents, {title = "GM Bar", renderer = renderGmBarSettings})
table.insert(state.settingsComponents, {title = "Character", renderer = renderCharacterSettings})
table.insert(state.settingsComponents, {title = "Stat Bars", renderer = renderStatBars})
table.insert(state.settingsComponents, {title = "Exp Bars", renderer = renderExperienceBars})
table.insert(state.settingsComponents, {title = "Zone", renderer = renderZoneSettings})
table.insert(state.settingsComponents, {title = "Macro", renderer = renderMacroSettings})
table.insert(state.settingsComponents, {title = "Time", renderer = renderTimeSettings})
table.insert(state.settingsComponents, {title = "Target", renderer = renderTargetSettings})
table.insert(state.settingsComponents, {title = "Clipboard", renderer = renderClipboardSettings})
table.insert(state.settingsComponents, {title = "Display Order", renderer = renderDisplayOrderSettings})
table.insert(state.settingsComponents, {title = "Advanced", renderer = renderAdvancedSettings})

local PROJECT_LINKS = {
    { title = ICON.MD_LANGUAGE.." Resource Page @ Redguides", url = "https://www.redguides.com/community/resources/quick-hud.2450/"},
    { title = ICON.FA_GIT.." Code Repository", url = "https://gitlab.com/anonymousbitshifter/quickhud"}
}

function QuickHudSettingsUI()
    if not state.openSettingsUI then return end
    ImGui.PushStyleVar(ImGuiStyleVar.Alpha, state.settings.contentOpacity)
    ImGui.PushStyleVar(ImGuiStyleVar.FrameRounding, 3)
    ImGui.PushStyleVar(ImGuiStyleVar.GrabRounding, 3)
    ImGui.SetNextWindowBgAlpha(state.settings.windowOpacity)
    state.openSettingsUI, state.shouldDrawSettingsUI = ImGui.Begin(ICON.MD_SETTINGS..' Quick HUD Settings', state.openSettingsUI, ImGuiWindowFlags.NoCollapse + ImGuiWindowFlags.MenuBar)
    if state.shouldDrawSettingsUI then

        if ImGui.BeginMenuBar() then
            if ImGui.BeginMenu(ICON.FA_LINK.." Links") then
                for index, linkItem in ipairs(PROJECT_LINKS) do
                    if ImGui.MenuItem(linkItem.title.."##projectLink"..tostring(index)) then
                        os.execute(string.format("start %s", linkItem.url))
                    end
                    uiComponents.ToolTip(string.format("click to open %s", linkItem.url))
                end
                ImGui.EndMenu()
            end
            ImGui.EndMenuBar()
        end

        for index, tab in ipairs(state.settingsComponents) do
            if ImGui.CollapsingHeader(tab.title) then
                tab.renderer(state)
            end
        end

        if appSettingsService:DoSettingsDiffer(state.settings, state.settingsBuffer) then
            utils.debounce(function()
                -- Settings have changed, timed to save.
                log.Debug("saving state.settings...")
                state.settings = appSettingsService:SaveSettings(state.settings)
                state.settingsBuffer = utils.copyTable(state.settings)
            end, "QuickHUDSaveSettings", 5)
        end

    end
    ImGui.End()
    ImGui.PopStyleVar(3)
    ImGui.SetNextWindowBgAlpha(1)
end

function renderSettingsMenuButton()
    if state.openSettingsUI then return end
    if ImGui.Button(ICON.MD_SETTINGS.." Quick HUD Settings") then
        state.openSettingsUI = true
    end
    ImGui.SameLine()
    if ImGui.Button(ICON.FA_REFRESH.." Reload Settings") then
        state.settings = appSettingsService:LoadAppSettings()
        state.settingsBuffer = utils.copyTable(state.settings)
    end
end

function register()
    mq.imgui.init('QuickHudSettingsUI', QuickHudSettingsUI)
    mq.bind('/qhudsettings', function()
        state.openSettingsUI = not state.openSettingsUI
    end)
end

return {
    renderSettingsMenuButton = renderSettingsMenuButton,
    register = register
}
