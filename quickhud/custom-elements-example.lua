return {
    ["columns"] = {
        [2] = {
            ["category"] = "1. Label";
            ["title"] = "Concatenated label";
            ["mode"] = 1;
            ["fontScale"] = 1;
            ["type"] = 1;
            ["scriptType"] = 1;
            ["buttonScriptType"] = 2;
            ["evalButtonLabel"] = true;
            ["value"] = "\"I am \" .. mq.TLO.Me.CleanName() \
.. \" a \"\
.. mq.TLO.Me.Class()\
.. \" of the \"\
.. mq.TLO.Me.Level() \
.. \"th level \"";
            ["id"] = "27a4efad-52b5-4c55-a5cf-87574acd8626";
            ["useDisplayCondition"] = false;
            ["displayCondition"] = "mq.TLO.Target.ID()";
            ["buttonSize"] = 1;
            ["buttonScriptType"] = 2;
            ["evalButtonLabel"] = true;
            ["buttonColor"] = {
                [1] = 0.24;
                [2] = 0.41;
                [3] = 0.63;
                [4] = 1;
            };
            ["buttonLabel"] = "'Button'";
            ["buttonScript"] = "mq.cmd('/echo hello world')";
        };
        [5] = {
            ["category"] = "2. Button";
            ["title"] = "Conditionally displayed button with formatted label";
            ["evalButtonLabel"] = true;
            ["buttonScriptType"] = 2;
            ["buttonSize"] = 3;
            ["fontScale"] = 1;
            ["buttonColor"] = {
                [1] = 0.8633759021759;
                [2] = 0.0034245378337801;
                [3] = 0.0034245378337801;
            };
            ["scriptType"] = 1;
            ["buttonLabel"] = "string.format('>> Burn %s <<', mq.TLO.Target.CleanName())";
            ["id"] = "42b118e8-a704-4212-bb80-0444d6f84aae";
            ["buttonWidth"] = 200;
            ["value"] = "mq.TLO.Me.Level()";
            ["buttonScript"] = "mq.cmd(\"/echo /burn command\")";
            ["buttonHeight"] = 50;
            ["type"] = 2;
            ["useDisplayCondition"] = true;
            ["displayCondition"] = "mq.TLO.Target.Named()";
            ["mode"] = 2;
        };
        [3] = {
            ["mode"] = 2;
            ["category"] = "1. Label";
            ["title"] = "Label with conditional display";
            ["buttonScriptType"] = 2;
            ["evalButtonLabel"] = false;
            ["id"] = "127acf71-e4f9-4e7a-974a-25553825d1a0";
            ["buttonColor"] = {
                [1] = 0.23999999463558;
                [2] = 0.40999999642372;
                [3] = 0.62999999523163;
            };
            ["scriptType"] = 1;
            ["buttonLabel"] = "Greet";
            ["buttonSize"] = 1;
            ["buttonScript"] = "mq.cmd(\"/echo hello\")";
            ["value"] = "mq.TLO.Target.CleanName()";
            ["type"] = 1;
            ["useDisplayCondition"] = false;
            ["displayCondition"] = "mq.TLO.Target.ID()";
            ["fontScale"] = 1;
        };
        [7] = {
            ["category"] = "2. Button";
            ["title"] = "Button that displays and selects the nearest NPC";
            ["evalButtonLabel"] = true;
            ["buttonScriptType"] = 3;
            ["fontScale"] = 1;
            ["buttonColor"] = {
                [1] = 0;
                [2] = 0.43589782714844;
                [3] = 1;
            };
            ["scriptType"] = 1;
            ["buttonLabel"] = "'NearestNPC: ' .. mq.TLO.NearestSpawn(\"npc\").CleanName()";
            ["buttonSize"] = 1;
            ["mode"] = 1;
            ["buttonScript"] = "/echo Targeting nearest NPC\
/target id ${NearestSpawn[npc].ID}";
            ["value"] = "ICON.MD_INFO .. ' Right click to edit'";
            ["type"] = 2;
            ["useDisplayCondition"] = false;
            ["displayCondition"] = "mq.TLO.Target.ID()";
            ["id"] = "2764ab11-49d8-4006-8334-f20dc2ada415";
        };
        [8] = {
            ["category"] = "3. Advanced";
            ["title"] = "Custom UI Example";
            ["mode"] = 2;
            ["buttonScriptType"] = 2;
            ["evalButtonLabel"] = true;
            ["scriptType"] = 2;
            ["fontScale"] = 0.75;
            ["type"] = 1;
            ["useDisplayCondition"] = false;
            ["value"] = "if mq.TLO.Target.ID() then\
ui.TextColored(\"Targeting: \" .. mq.TLO.Target.CleanName(), COLOR.LIMEGREEN)\
if ImGui.SmallButton(\"say hello to \" .. mq.TLO.Target.CleanName()) then\
\9mq.cmd(\"/say Hello \" .. mq.TLO.Target.CleanName())\
end\
else\
  ImGui.Text(\"No Target Selected\")\
end";
            ["id"] = "27a4efad-52b5-4c55-a5cf-87574acd8604";
        };
        [4] = {
            ["category"] = "1. Label";
            ["title"] = "Multiline label that displays Pets and PCs in zone";
            ["mode"] = 1;
            ["buttonScriptType"] = 2;
            ["evalButtonLabel"] = true;
            ["buttonSize"] = 1;
            ["buttonColor"] = {
                [1] = 0.24;
                [2] = 0.41;
                [3] = 0.63;
                [4] = 1;
            };
            ["scriptType"] = 1;
            ["buttonLabel"] = "'Button'";
            ["fontScale"] = 1;
            ["buttonScript"] = "mq.cmd('/echo hello world')";
            ["value"] = "\"Pets: \" .. mq.TLO.SpawnCount(\"pet\")()\
.. \"\\n\"\
.. \"PCs: \" .. mq.TLO.SpawnCount(\"pc\")()";
            ["type"] = 1;
            ["useDisplayCondition"] = false;
            ["displayCondition"] = "mq.TLO.Target.ID()";
            ["id"] = "1494c152-8e4c-4109-a657-41efa364263e";
        };
        [1] = {
            ["category"] = "1. Label";
            ["title"] = "Label that displays the current zone";
            ["mode"] = 1;
            ["buttonScriptType"] = 2;
            ["evalButtonLabel"] = true;
            ["buttonSize"] = 1;
            ["buttonColor"] = {
                [1] = 0.24;
                [2] = 0.41;
                [3] = 0.63;
                [4] = 1;
            };
            ["scriptType"] = 1;
            ["buttonLabel"] = "'Button'";
            ["id"] = "d3f8f6cf-e084-4979-8b72-9b1b6375a8f3";
            ["buttonScript"] = "mq.cmd('/echo hello world')";
            ["value"] = "mq.TLO.Zone()";
            ["type"] = 1;
            ["useDisplayCondition"] = false;
            ["displayCondition"] = "mq.TLO.Target.ID()";
            ["fontScale"] = 1;
        };
        [6] = {
            ["category"] = "2. Button";
            ["title"] = "Button with formatted label that executes a command";
            ["mode"] = 2;
            ["buttonScriptType"] = 2;
            ["evalButtonLabel"] = true;
            ["buttonSize"] = 1;
            ["buttonColor"] = {
                [1] = 0.62999999523163;
                [2] = 0.2400000244379;
                [3] = 0.52773034572601;
            };
            ["scriptType"] = 1;
            ["buttonLabel"] = "ICON.MD_WARNING .. ' EVAC'";
            ["fontScale"] = 1.5;
            ["buttonScript"] = "mq.cmd('/echo /evac command')";
            ["value"] = "ICON.MD_INFO .. ' Right click to edit'";
            ["type"] = 2;
            ["useDisplayCondition"] = false;
            ["displayCondition"] = "mq.TLO.Target.ID()";
            ["id"] = "8613c04d-1b17-4d70-bceb-80e6af2cda57";
        };
        [9] = {
            ["category"] = "3. Advanced";
            ["title"] = "List of player buffs";
            ["evalButtonLabel"] = false;
            ["buttonScriptType"] = 2;
            ["id"] = "0c3c9686-40ea-4380-b0b2-7cf6b5529e21";
            ["buttonColor"] = {
                [1] = 0.24;
                [2] = 0.41;
                [3] = 0.63;
                [4] = 1;
            };
            ["scriptType"] = 2;
            ["buttonLabel"] = "Button";
            ["fontScale"] = 0.75;
            ["buttonSize"] = 1;
            ["buttonScript"] = "mq.cmd('/echo hello world')";
            ["value"] = "local TEXT_BASE_WIDTH = ImGui.CalcTextSize(\"A\")\
local TEXT_BASE_HEIGHT = ImGui.GetTextLineHeightWithSpacing()\
local animSpellIcons = mq.FindTextureAnimation('A_SpellIcons')\
local iconSize = 24\
local hasBuffs = false\
\
local borderColor = COLOR.withAlpha(COLOR.DODGERBLUE, 0.75)\
\
local function dummy()\
ImGui.Dummy(iconSize, iconSize)\
end\
\
ui.StartWrapPanel(iconSize)\
\
for index = 1, mq.TLO.Me.MaxBuffSlots() do\
local spell = mq.TLO.Me.Buff(index)\
if spell() then\
ui.NextWrapPanelItem()\
hasBuffs = true\
\
local xPos = ImGui.GetCursorPosX() + 1\
local yPos = ImGui.GetCursorPosY() + 1\
\
ImGui.BeginGroup()\
\
ImGui.PushStyleVar(ImGuiStyleVar.FrameBorderSize, 2)\
ImGui.PushStyleColor(ImGuiCol.Border, borderColor[1], borderColor[2], borderColor[3], borderColor[4] or 1)\
\
ui.renderWithBackground(dummy, {\
width = iconSize + 2,\
height = iconSize + 2,\
backgroundColor = {0, 0, 0, 0.15}\
})\
\
ImGui.SetCursorPosX(xPos)\
ImGui.SetCursorPosY(yPos)\
\
animSpellIcons:SetTextureCell(spell.SpellIcon())\
ImGui.DrawTextureAnimation(animSpellIcons, iconSize, iconSize)\
\
ImGui.PopStyleVar()\
ImGui.PopStyleColor()\
\
ImGui.SameLine()\
\
local durationLabel = spell.Duration.TotalSeconds() .. \"s\"\
if spell.Duration.TotalMinutes() > 0 then\
durationLabel = spell.Duration.TotalMinutes() .. \"m\"\
end\
if spell.Duration.TotalMinutes() > 60 then\
if spell.Duration.TotalMinutes() / 60 > 100 then\
    durationLabel = \"Permanent\"\
else\
    durationLabel = string.format(\"%d\", spell.Duration.TotalMinutes() / 60) .. \"h\"\
end\
end\
\
local height = ImGui.GetFontSize()\
\
if durationLabel ~= \"Permanent\" then\
ImGui.SetCursorPosX(xPos)\
\
ImGui.SetCursorPosY(yPos + iconSize - height)\
\
ui.renderWithBackground(durationLabel, {\
    width = iconSize,\
    height = height,\
    backgroundColor = {0, 0, 0, 0.15},\
    paddingY = 1\
})\
end\
\
ImGui.EndGroup()\
\
\
\
ui.ToolTip(function()\
animSpellIcons:SetTextureCell(spell.SpellIcon())\
ImGui.DrawTextureAnimation(animSpellIcons, 32, 32)\
ImGui.SameLine()\
ImGui.BeginGroup()\
ImGui.Text(string.format(\"%s (lvl %s)\", spell.Name(), spell.Level()))\
ImGui.Text(\"Remaining Duration: \" .. ((durationLabel == \"Permanent\") and durationLabel or spell.Duration.TimeHMS()))\
ImGui.EndGroup()\
ImGui.Separator()\
if spell.Category() ~= \"Unknown\" then\
    ImGui.Text(spell.Category())\
end\
if spell.Subcategory() ~= \"Unknown\" then\
    ImGui.Text(ICON.MD_SUBDIRECTORY_ARROW_RIGHT .. spell.Subcategory())\
end\
-- if spell.CastTime() and spell.MyCastTime() then\
--     ImGui.Text(string.format(\"Cast Time: %s (%s)\" spell.CastTime(), spell.MyCastTime()))\
-- end\
-- ImGui.Text(string.format(\"Cast Time: %s (%s)\" spell.CastTime(), spell.MyCastTime()))\
if spell.CounterType() and spell.CounterType() ~= \"None\" then\
    ImGui.Text(\"Counter: \" .. spell.CounterType())\
end\
end)\
\
ImGui.SetCursorPosY(yPos + iconSize + 2)\
end\
end\
\
ui.EndWrapPanel()\
\
if not hasBuffs then\
ImGui.Text(\"<no buffs>\")\
end\
";
            ["type"] = 1;
            ["useDisplayCondition"] = false;
            ["displayCondition"] = "mq.TLO.Target.ID()";
            ["mode"] = 1;
        };
        [10] = {
            ["category"] = "3. Advanced";
            ["title"] = "List of target buffs";
            ["evalButtonLabel"] = false;
            ["buttonScriptType"] = 2;
            ["fontScale"] = 0.69999998807907;
            ["buttonColor"] = {
                [1] = 0.24;
                [2] = 0.41;
                [3] = 0.63;
                [4] = 1;
            };
            ["scriptType"] = 2;
            ["buttonLabel"] = "Button";
            ["mode"] = 1;
            ["buttonSize"] = 1;
            ["buttonScript"] = "mq.cmd('/echo hello world')";
            ["value"] = "local TEXT_BASE_WIDTH = ImGui.CalcTextSize(\"A\")\
    local TEXT_BASE_HEIGHT = ImGui.GetTextLineHeightWithSpacing()\
    local animSpellIcons = mq.FindTextureAnimation('A_SpellIcons')\
    local iconSize = 24\
    local hasBuffs = false\
    \
    ImGui.Text(mq.TLO.Target.CleanName())\
    \
    ui.StartWrapPanel(iconSize)\
    \
    for index = 1, 255 do\
    local spell = mq.TLO.Target.Buff(index)\
    if spell() then\
    ui.NextWrapPanelItem()\
    hasBuffs = true\
    \
    local xPos = ImGui.GetCursorPosX()\
    local yPos = ImGui.GetCursorPosY()\
    \
    ImGui.BeginGroup()\
    \
    animSpellIcons:SetTextureCell(spell.SpellIcon())\
    ImGui.DrawTextureAnimation(animSpellIcons, iconSize, iconSize)\
    \
    ImGui.SameLine()\
    \
    local durationLabel = spell.Duration.TotalSeconds() .. \"s\"\
    if spell.Duration.TotalMinutes() > 0 then\
    durationLabel = spell.Duration.TotalMinutes() .. \"m\"\
    end\
    if spell.Duration.TotalMinutes() > 60 then\
    durationLabel = string.format(\"%d\", spell.Duration.TotalMinutes() / 60) .. \"h\"\
    end\
    local bufferSpaceSize = (ImGui.CalcTextSize(durationLabel) - (TEXT_BASE_WIDTH * -3))\
    ImGui.SetCursorPosX(xPos)\
    \
    ImGui.SetCursorPosY(yPos + iconSize - TEXT_BASE_HEIGHT)\
    \
    local overlayColor = {0, 0, 0, 0.45}\
    ImGui.PushStyleColor(ImGuiCol.PlotHistogram, overlayColor[1] or 0, overlayColor[2] or 0, overlayColor[3] or 0, overlayColor[4] or 0.5)\
    ImGui.ProgressBar(1.0, iconSize, TEXT_BASE_HEIGHT, durationLabel)\
    ImGui.PopStyleColor()\
    \
    ImGui.EndGroup()\
    ImGui.SetCursorPosY(yPos + iconSize + 2)\
    ui.ToolTip(spell.Name() .. \"\\nRemaining Duration: \" .. durationLabel)\
    end\
    end\
    \
    ui.EndWrapPanel()\
    \
    if not hasBuffs then\
    ImGui.Text(\"<no buffs>\")\
    end";
            ["type"] = 1;
            ["useDisplayCondition"] = false;
            ["displayCondition"] = "mq.TLO.Target.ID()";
            ["id"] = "d2df6f0b-436a-4c9a-b54c-06e1e949f67c";
        };
        [11] = {
            ["category"] = "3. Advanced";
            ["title"] = "Player casting bar";
            ["evalButtonLabel"] = false;
            ["buttonScriptType"] = 2;
            ["fontScale"] = 1;
            ["buttonColor"] = {
                [1] = 0.24;
                [2] = 0.41;
                [3] = 0.63;
                [4] = 1;
            };
            ["scriptType"] = 2;
            ["mode"] = 1;
            ["buttonSize"] = 1;
            ["id"] = "b1a58a6e-307e-465d-8c76-097814eda8ca";
            ["buttonScript"] = "mq.cmd('/echo hello world')";
            ["value"] = "local spell = mq.TLO.Me.Casting\
local castTimeLeft = mq.TLO.Me.CastTimeLeft()\
\
local backgroundColor = COLOR.withAlpha(COLOR.BLACK, 0.05)\
local barColor = COLOR.withAlpha(COLOR.MAGENTA, 0.5)\
local borderColor = COLOR.DODGERBLUE\
local borderWidth = 0.15\
\
local showName = true\
local showCastingTime = true\
\
local remainingCastTime = castTimeLeft / 1000\
local myCastTime = spell.MyCastTime() / 1000\
\
if remainingCastTime > myCastTime then\
remainingCastTime = myCastTime\
end\
\
local castTimeLabel = string.format(\"(%.2fs / %.2fs)\", remainingCastTime, myCastTime)\
local label = string.format(\"%s %s\", showName and spell.Name() or \"\", showCastingTime and castTimeLabel or \"\")\
\
local function renderCastBar()\
ui.renderWithBackground(label, {\
width = ui.GetAvailableX() * utils.clamp(castTimeLeft / spell.CastTime(), 0, 1),\
height = TEXT_BASE_HEIGHT,\
backgroundColor = barColor,\
paddingX = 4,\
paddingY = 2\
})\
end\
\
ImGui.PushStyleVar(ImGuiStyleVar.FrameBorderSize, 0.15)\
ImGui.PushStyleColor(ImGuiCol.Border, borderColor[1], borderColor[2], borderColor[3], borderColor[4] or 1)\
\
ui.renderWithBackground(renderCastBar, {\
width = ui.GetAvailableX(),\
height = TEXT_BASE_HEIGHT,\
backgroundColor = backgroundColor\
})\
ui.ToolTip(label)\
\
ImGui.PopStyleColor()\
ImGui.PopStyleVar()\
";
            ["type"] = 1;
            ["useDisplayCondition"] = true;
            ["displayCondition"] = "mq.TLO.Me.Casting()";
            ["buttonLabel"] = "Button";
        };
        [12] = {
            ["category"] = "3. Advanced";
            ["title"] = "Worn items";
            ["evalButtonLabel"] = false;
            ["buttonScriptType"] = 2;
            ["buttonSize"] = 1;
            ["buttonColor"] = {
                [1] = 0.24;
                [2] = 0.41;
                [3] = 0.63;
                [4] = 1;
            };
            ["scriptType"] = 2;
            ["mode"] = 1;
            ["buttonLabel"] = "Button";
            ["fontScale"] = 1;
            ["buttonScript"] = "mq.cmd('/echo hello world')";
            ["value"] = "local animItems = mq.FindTextureAnimation(\"A_DragItem\")\
    local animBox = mq.FindTextureAnimation(\"A_RecessedBox\")\
    local renderTooltip = require(\"quickhud.item-tooltips\")\
    local WORN_SLOT_NAMES = { \"Charm\", \"Left Ear\", \"Head\", \"Face\", \"Right Ear\", \"Neck\", \"Shoulder\", \"Arms\", \"Back\", \"Left Wrist\", \"Right Wrist\", \"Ranged\", \"Hands\", \"Main Hand\", \"Off Hand\", \"Left Finger\", \"Right Finger\", \"Chest\", \"Legs\", \"Feet\", \"Waist\", \"Power Source\", \"Ammo\" }\
    \
    local boxSize = 20\
    local paddingX = 2\
    local paddingY = 2\
    local iconSize = boxSize - (paddingX + paddingY)\
    \
    ui.StartWrapPanel(boxSize)\
    \
    for index, label in ipairs(WORN_SLOT_NAMES) do\
    ui.NextWrapPanelItem()\
    \
    if index == (#WORN_SLOT_NAMES - 1) then\
    break\
    end\
    \
    local yPos = ImGui.GetCursorPosY()\
    \
    ImGui.BeginGroup()\
    \
    local item = mq.TLO.Me.Inventory(index - 1)\
    local xPos = ImGui.GetCursorPosX()\
    ImGui.DrawTextureAnimation(animBox, boxSize, boxSize)\
    ImGui.SameLine()\
    ImGui.SetCursorPosX(xPos + paddingX)\
    ImGui.SetCursorPosY(yPos + paddingY)\
    \
    animItems:SetTextureCell(item.Icon() - 500)\
    ImGui.DrawTextureAnimation(animItems, iconSize, iconSize)\
    \
    ImGui.EndGroup()\
    \
    renderTooltip(item)\
    end\
    \
    ui.EndWrapPanel()";
            ["type"] = 1;
            ["useDisplayCondition"] = false;
            ["displayCondition"] = "mq.TLO.Target.ID()";
            ["id"] = "45ceab5f-c76e-4c6f-9760-9f265ff5ec9e";
        };
        [13] = {
            ["category"] = "3. Advanced";
            ["title"] = "Wearable items in inventory";
            ["evalButtonLabel"] = false;
            ["buttonScriptType"] = 2;
            ["buttonSize"] = 1;
            ["buttonColor"] = {
                [1] = 0.24;
                [2] = 0.41;
                [3] = 0.63;
                [4] = 1;
            };
            ["scriptType"] = 2;
            ["mode"] = 1;
            ["fontScale"] = 1;
            ["buttonLabel"] = "Button";
            ["buttonScript"] = "mq.cmd('/echo hello world')";
            ["value"] = "local animItems = mq.FindTextureAnimation(\"A_DragItem\")\
local animBox = mq.FindTextureAnimation(\"A_RecessedBox\")\
local renderTooltip = require(\"quickhud.item-tooltips\")\
local WORN_SLOT_NAMES = { \"Charm\", \"Left Ear\", \"Head\", \"Face\", \"Right Ear\", \"Neck\", \"Shoulder\", \"Arms\", \"Back\", \"Left Wrist\", \"Right Wrist\", \"Ranged\", \"Hands\", \"Main Hand\", \"Off Hand\", \"Left Finger\", \"Right Finger\", \"Chest\", \"Legs\", \"Feet\", \"Waist\", \"Power Source\", \"Ammo\" }\
\
local function isWearable(item)\
    local wearableTypes = {\
        [\"Armor\"] = true,\
        [\"Jewelry\"] = true,\
        [\"1H Slashing\"] = true,\
        [\"1H Blunt\"] = true,\
        [\"1H Piercing\"] = true,\
        [\"2H Slashing\"] = true,\
        [\"2H Blunt\"] = true,\
        [\"2H Piercing\"] = true,\
        [\"Charm\"] = true\
    }\
\
    return wearableTypes[item.Type()] and (item.WornSlots() > 0)\
end\
\
local boxSize = 20\
local paddingX = 2\
local paddingY = 2\
local iconSize = boxSize - (paddingX + paddingY)\
\
function drawItem(item)\
    if item() and isWearable(item) then\
        ui.NextWrapPanelItem()\
        local yPos = ImGui.GetCursorPosY()\
\
        ImGui.BeginGroup()\
    \
        local xPos = ImGui.GetCursorPosX()\
        ImGui.DrawTextureAnimation(animBox, boxSize, boxSize)\
        ImGui.SameLine()\
        ImGui.SetCursorPosX(xPos + paddingX)\
        ImGui.SetCursorPosY(yPos + paddingY)\
    \
        animItems:SetTextureCell(item.Icon() - 500)\
        ImGui.DrawTextureAnimation(animItems, iconSize, iconSize)\
    \
        ImGui.EndGroup()\
\
        if ImGui.IsKeyDown(16) then\
            renderTooltip(item)\
        else\
            ui.ToolTip(\"Hold shift key to see comparison tooltip\")\
        end\
    end\
end\
\
function drawContainerItems(panelId, containerSize, containerSlotIndex)\
    for slot = 1, containerSize, 1 do\
        local item = mq.TLO.Me.Inventory(containerSlotIndex).Item(slot)\
        drawItem(item)\
    end\
end\
\
local bagStartIndex = 23\
local bagEndIndex = 32\
\
local panelId = ui.StartWrapPanel(boxSize)\
for bag = bagStartIndex, bagEndIndex do\
    if mq.TLO.Me.Inventory(bag).Container() and mq.TLO.Me.Inventory(bag).Container() > 0 then\
        drawContainerItems(panelId, mq.TLO.Me.Inventory(bag).Container(), bag)\
    else\
        drawItem(mq.TLO.Me.Inventory(bag))\
    end\
end\
ui.EndWrapPanel()";
            ["type"] = 1;
            ["useDisplayCondition"] = false;
            ["displayCondition"] = "mq.TLO.Target.ID()";
            ["id"] = "8b8654a9-e78e-47df-9f40-7d65f972f38b";
        };
    };
    ["id"] = "713c502f-fe55-429d-89b7-5083915c4e6b";
};
