--- @type Mq
local mq = require('mq')

--- @type ImGui
require 'ImGui'

-- Logging
local log = require('quickhud.lib.Write')

-- Libraries
local ICON = require('quickhud.lib.icons')
local utils = require('quickhud.utils')
local uiComponents = require('quickhud.uihelpers')
local state = require('quickhud.state')
local COLOR = require('quickhud.lib.color')
local services = require('quickhud.services')
local linq = require("quickhud.lib.lazylualinq.linq")

local exampleRow = require("quickhud.custom-elements-example")

local appSettingsService = services.AppSettings

local ScriptType = {
    Inline = 1,
    Advanced = 2,
    EQ = 3
}

local WidgetType = {
    Label = 1,
    Button = 2
}

local ButtonSize = {
    Small = 1,
    Normal = 2,
    Custom = 3
}

local WidgetMode = {
    Basic = 1,
    Advanced = 2
}

local SCRIPT_STATE_IMPORT_TEMPLATE = [[
    local elementData = require('quickhud.custom-elements-data')
    local ___widgetId = '%s'

    local hud = {
        getState = function(defaultValues)
            return elementData.getState(___widgetId, defaultValues)
        end,
        setState = function(value)
            elementData.setState(___widgetId, value)
        end
    }
]]

local SCRIPT_TEMPLATE = [[
    local mq = require('mq');
    local ICON = require('quickhud.lib.icons');
    local COLOR = require('quickhud.lib.color');
    local ui = require('quickhud.uihelpers')
    local utils = require('quickhud.utils')
    require 'ImGui'

    return %s;
]]

local IIFE_SCRIPT_TEMPLATE = string.format(SCRIPT_TEMPLATE,
    [[(function()
        %s
    end)()]]
)

SCRIPT_TEMPLATE = SCRIPT_STATE_IMPORT_TEMPLATE .. SCRIPT_TEMPLATE
IIFE_SCRIPT_TEMPLATE = SCRIPT_STATE_IMPORT_TEMPLATE .. IIFE_SCRIPT_TEMPLATE

local BUTTON_STANDARD_SCRIPT_HELP_TEXT = "Standard Script: Button will execute one or more lua statements when clicked.\n\tIE: mq.cmd('/echo I am a button!')"
local BUTTON_EQ_SCRIPT_HELP_TEXT = "EQ Script: Button will execute one or more basic EQ / MQ slash commands when clicked.\n\tIE: /echo ${Me.Level}\n\tIE: /dggae /say hello"
local BASIC_SCRIPT_HELP_TEXT = "Basic will parse and return the value from the lua script as you would would expect through /lua parse\n\tIE: mq.TLO.Me.Level()"
local ADVANCED_SCRIPT_HELP_TEXT = [[Advanced will wrap the lua script in an IIFE and will not produce a return value
    Using advanced you can use Lua & ImGui functions create your own UI and functionality.
    Using ImGui constructs improperly can destabilize your game, use with caution.]]

local SCRIPT_HELP_TEXT = string.format([[Determines the execution context of the script

%s

%s]], BASIC_SCRIPT_HELP_TEXT, ADVANCED_SCRIPT_HELP_TEXT)

local DEFAULT_BUTTON_COLOR = { utils.hex("#3F6BA3", 1) }

local NEW_WIDGET_TEMPLATE = {
    value = "ICON.MD_INFO .. ' Right-click to edit'",
    type = WidgetType.Label,
    scriptType = ScriptType.Inline,
    buttonLabel = "Button",
    displayCondition = "mq.TLO.Target.ID()",
    useDisplayCondition = false,
    buttonSize = ButtonSize.Small,
    buttonScript = "mq.cmd('/echo hello world')",
    buttonColor = DEFAULT_BUTTON_COLOR,
    mode = WidgetMode.Basic,
    buttonScriptType = ScriptType.Advanced,
    evalButtonLabel = false
}

local EXAMPLES_POPUP_WINDOW_HANDLE = "quickhudExampleWidgets"

local hovering = {}
local editing = {}

local function createWidget()
    local widget = utils.copyTable(NEW_WIDGET_TEMPLATE)
    widget.id = utils.uuid()
    return widget
end

local function eval(luaString)
    local success, result = pcall(loadstring, luaString)
    if success then
        if type(result) == 'function' then
            success, result = pcall(result)
            return success, result
        end
    end
    return success, result
end

local function save()
    appSettingsService:SaveSettings(state.settings)
end

local function renderCustomButtonElement(column, template)
    local buttonLabel = column.buttonLabel
    ImGui.BeginGroup()

    if column.evalButtonLabel then
        local scriptString = string.format(SCRIPT_TEMPLATE, column.id, column.buttonLabel)
        local success, result = eval(scriptString)
        buttonLabel = result or ""

        if not success then
            uiComponents.TextColored(string.format("%s %s", ICON.MD_ERROR_OUTLINE, result or "Error"), COLOR.ORANGERED)
            uiComponents.ToolTip(scriptString)
            ImGui.EndGroup()
            return
        end
    end

    local buttonLabelWithId = buttonLabel .. "##" .. column.id

    local clicked = false
    if uiComponents.ButtonColored(buttonLabelWithId,
        column.buttonColor,
        column.buttonSize == ButtonSize.Small,
        column.buttonSize == ButtonSize.Custom and column.buttonWidth or nil,
        column.buttonSize == ButtonSize.Custom and column.buttonHeight or nil) then

            if column.buttonScriptType == ScriptType.EQ then
                local lines = utils.split(column.buttonScript, "\n")
                state.dispatch(function()
                    for index, line in ipairs(lines) do
                        utils.safeCall(mq.cmd, string.format("%s: %s", buttonLabel, line), {line})
                    end
                end)
            else
                state.dispatch(function()
                    local scriptString = string.format(IIFE_SCRIPT_TEMPLATE, column.id, column.buttonScript)
                    local success, result = eval(scriptString)

                    if not success then
                        log.Error(string.format("There was an error running the custom script for the widget button '%s'", buttonLabel))
                        log.Error(result)
                    end
                end)
            end
    end

    uiComponents.ToolTip(buttonLabel)

    ImGui.EndGroup()
end

local function renderAdvancedScriptElement(column)
    ImGui.BeginGroup()
    local template = IIFE_SCRIPT_TEMPLATE

    local scriptString = string.format(template, column.id, column.value)

    ImGui.BeginGroup()

    local success, result = eval(scriptString)

    if not success then
        uiComponents.TextColored(string.format("%s %s", ICON.MD_ERROR_OUTLINE, result or "Error"), COLOR.ORANGERED)
        uiComponents.ToolTip(scriptString)
    end

    ImGui.EndGroup()

    if result then
        uiComponents.ToolTip(result)
    end

    ImGui.EndGroup()
end

local function renderCustomLabelElement(column, template)
    if column.scriptType == ScriptType.Advanced then
        renderAdvancedScriptElement(column)
    else
        local scriptString = string.format(SCRIPT_TEMPLATE, column.id, column.value)

        ImGui.BeginGroup()

        local success, result = eval(scriptString)

        if not success then
            uiComponents.TextColored(string.format("%s %s", ICON.MD_ERROR_OUTLINE, result or "Error"), COLOR.ORANGERED)
            uiComponents.ToolTip(scriptString)
        else
            local resultString = tostring(result or "<empty>")
            ImGui.Text(resultString)
            uiComponents.ToolTip(resultString)
        end

        ImGui.EndGroup()
    end
end

local function renderCustomElement(column)
    if column.useDisplayCondition then
        local success, result = eval(string.format(SCRIPT_TEMPLATE, column.id, column.displayCondition))
        if not success or not result then
            if column.type == WidgetType.Button and column.buttonSize == ButtonSize.Custom then
                ImGui.Dummy(column.buttonWidth, column.buttonHeight)
            end

            return
        end
    end

    if column.type == WidgetType.Button then
        return renderCustomButtonElement(column)
    end

    renderCustomLabelElement(column)
end

local function renderCustomElementEditForm(column, columns, columnIndex, rowIndex, removeColumn, saveColumn, cancel)
    ImGui.BeginGroup()
    local columnId = column.id

    ImGui.Text("Type")
    column.type = ImGui.RadioButton("Label##widgetType", column.type or WidgetType.Label, WidgetType.Label)
    ImGui.SameLine()
    column.type = ImGui.RadioButton("Button##widgetType", column.type or WidgetType.Label, WidgetType.Button)

    ImGui.NewLine()

    if column.type == WidgetType.Button then
        ImGui.Text("Button Label")
        column.buttonLabel = ImGui.InputText("##widgetLabel", column.buttonLabel or "", ImGuiInputTextFlags.EnterReturnsTrue)
        column.evalButtonLabel = uiComponents.DrawCheckBox("Evaluate label as script?", "##widgetAllowLuaInButtonLabel", column.evalButtonLabel or false, "Allows you to use lua to define the button label.\nThis can be helpful if you want dynamic labeling based on game data or if you want to interpolate values like names, etc.\n\tIE: 'Targeting ' .. mq.TLO.Target.CleanName()")
        column.buttonColor = uiComponents.DrawColorEditor("Button Color", column.buttonColor or DEFAULT_BUTTON_COLOR)

        ImGui.NewLine()

        ImGui.Text("Size")
        column.buttonSize = ImGui.RadioButton("Small##buttonSize", column.buttonSize or ButtonSize.Small, ButtonSize.Small)
        ImGui.SameLine()
        column.buttonSize = ImGui.RadioButton("Normal##buttonSize", column.buttonSize or ButtonSize.Small, ButtonSize.Normal)
        ImGui.SameLine()
        column.buttonSize = ImGui.RadioButton("Custom##buttonSize", column.buttonSize or ButtonSize.Small, ButtonSize.Custom)

        if column.buttonSize == ButtonSize.Custom then
            column.buttonWidth = math.max(5, math.min(ImGui.InputInt("width##buttonSizeWidth", column.buttonWidth or 50, 1), 500))
            column.buttonHeight = math.max(5, math.min(ImGui.InputInt("height##buttonSizeHeight", column.buttonHeight or 50, 1), 200))
        end

        ImGui.NewLine()

        ImGui.Text("Script Type")
        ImGui.SameLine()
        uiComponents.HelpMarker(string.format("%s\n\n%s", BUTTON_STANDARD_SCRIPT_HELP_TEXT, BUTTON_EQ_SCRIPT_HELP_TEXT))

        column.buttonScriptType = ImGui.RadioButton("Standard##buttonScriptType", column.buttonScriptType or ScriptType.Advanced, ScriptType.Advanced)
        uiComponents.ToolTip(BUTTON_STANDARD_SCRIPT_HELP_TEXT)
        ImGui.SameLine()
        column.buttonScriptType = ImGui.RadioButton("EQ/MQ##buttonScriptType", column.buttonScriptType or ScriptType.Advanced, ScriptType.EQ)
        uiComponents.ToolTip(BUTTON_EQ_SCRIPT_HELP_TEXT)

        column.buttonScript, selected = ImGui.InputTextMultiline("##widgetButtonScript", column.buttonScript or "", -1, 100, 2000)
    else
        ImGui.Text("Script Type")
        ImGui.SameLine()
        uiComponents.HelpMarker(SCRIPT_HELP_TEXT)

        column.scriptType = ImGui.RadioButton("Basic##scriptType", column.scriptType or ScriptType.Inline, ScriptType.Inline)
        uiComponents.ToolTip(BASIC_SCRIPT_HELP_TEXT)
        ImGui.SameLine()
        column.scriptType = ImGui.RadioButton("Advanced##scriptType", column.scriptType or ScriptType.Inline, ScriptType.Advanced)
        uiComponents.ToolTip(ADVANCED_SCRIPT_HELP_TEXT)

        column.value, selected = ImGui.InputTextMultiline("##widgetValue", column.value or "", -1, 100, 2000)
    end

    ImGui.NewLine()

    column.useDisplayCondition = uiComponents.DrawCheckBox("Conditionally Display Widget", "##widgetConditionalDisplay", column.useDisplayCondition or false, "Only render the content of the widget if the specified condition returns a truthy value.\n\nThis can be helpful if for example you only want a button to display if you are targeting a named or some other condition.")
    if column.useDisplayCondition then
        ImGui.Text("Display Condition")
        column.displayCondition = ImGui.InputText("##widgetDisplayCondition", column.displayCondition or "", ImGuiInputTextFlags.EnterReturnsTrue)
    end

    ImGui.NewLine()

    ImGui.Text("Font Scale")
    column.fontScale = ImGui.SliderFloat("##widgetFontScale", column.fontScale or 1, 0.5, 3.0, "%.2f", 0.05)
    uiComponents.ToolTip(string.format("This will change the scale of the font (%.2f)\nThis is similar to changing the fontsize however we don't have that capabiltity so the text may become blurry at some scales.", column.fontScale))

    ImGui.NewLine()

    ImGui.Separator()

    if uiComponents.ButtonColored(ICON.MD_SAVE .. " Save##"..columnId, COLOR.BLUE) then
        saveColumn(column)
    end
    uiComponents.ToolTip("Save these changes")

    ImGui.SameLine()

    if ImGui.Button(ICON.MD_CANCEL .. " Cancel##"..columnId) then
        cancel()
    end
    uiComponents.ToolTip("Cancel these changes")

    ImGui.SameLine(uiComponents.GetAvailableX() - ((TEXT_BASE_WIDTH * 7) + 2))

    if uiComponents.ButtonColored(ICON.FA_TRASH .. " Delete##"..columnId, COLOR.ORANGERED) then
        removeColumn()
    end
    uiComponents.ToolTip("Remove the widget from the HUD")

    ImGui.Separator()

    ImGui.NewLine()
    ImGui.Text("Preview")
    ImGui.PushTextWrapPos(ImGui.GetCursorPosX() + (ImGui.GetFontSize() * 15.0))
    ImGui.SetWindowFontScale(column.fontScale or 1)
    renderCustomElement(column)
    ImGui.SetWindowFontScale(1)
    ImGui.PopTextWrapPos()

    ImGui.EndGroup()
end

local function renderCustomRowContextMenu(row, rows, rowIndex)
    if ImGui.BeginPopupContextItem("##widgetRowContextMenu"..tostring(rowIndex)) then
        if ImGui.MenuItem(ICON.FA_TRASH .. " Remove Row") then
            table.remove(rows, rowIndex)
            save()
        end
        uiComponents.ToolTip("Remove the widget row from the HUD")

        ImGui.EndPopup()
    end
end

local function renderExamplesPopup()
    if ImGui.BeginPopup(EXAMPLES_POPUP_WINDOW_HANDLE, ImGuiWindowFlags.NoResize) then
        local category = ""

        linq.from(exampleRow.columns)
            :orderBy(function(element, index) return element.category end)
            :thenBy(function(element, index) return element.title end)
            :foreach(function(index, element)
                if element.category ~= category then
                    category = element.category
                    ImGui.NewLine()
                    ImGui.Text(element.category)
                    ImGui.Separator()
                end

                if ImGui.Button(element.title .. "##exampleWidgetAddButton") then
                    local copy = utils.copyTable(element)
                    copy.id = utils.uuid()
                    table.insert(state.settings.customElements[#state.settings.customElements].columns, copy)
                    save()
                end
            end)

        ImGui.NewLine()

        ImGui.EndPopup()
    end
end

local function openColumnEditPopup(handle, column)
    ImGui.CloseCurrentPopup()
    editing.widget = utils.copyTable(column)
    ImGui.OpenPopup(handle)
end

local function displayCustomElements()
    local rows = state.settings.customElements

    ImGui.BeginGroup()
    for rowIndex, row in ipairs(rows) do
        ImGui.BeginGroup()

        local cursorPosX = ImGui.GetCursorPosX()
        ImGui.Dummy(uiComponents.GetAvailableX(), TEXT_BASE_HEIGHT * 0.8)
        ImGui.SameLine(cursorPosX)

        local columns = row.columns

        if #columns > 0 then
            ImGui.Columns(#columns + 1, row.id)

            for columnIndex, column in ipairs(columns) do
                ImGui.BeginGroup()
                ImGui.SetWindowFontScale(column.fontScale or 1)

                local cursorPosX = ImGui.GetCursorPosX()
                local cursorPosY = ImGui.GetCursorPosY()
                ImGui.Dummy(uiComponents.GetAvailableX(), TEXT_BASE_HEIGHT * 0.8)
                ImGui.SameLine()
                ImGui.SetCursorPosX(cursorPosX)

                ImGui.PushTextWrapPos(ImGui.GetCursorPosX() + uiComponents.GetAvailableX())

                utils.safeCall(renderCustomElement, string.format("renderCustomElement(%s)", column.value), {column})

                ImGui.PopTextWrapPos()

                ImGui.SetWindowFontScale(1)
                ImGui.EndGroup()

                local popupWindowHandle = "##editWidgetPopup" .. column.id

                if ImGui.IsItemClicked(ImGuiMouseButton.Right) then
                    openColumnEditPopup(popupWindowHandle, column)
                end

                if hovering[column.id] then
                    ImGui.SameLine(uiComponents.GetAvailableX() - ((TEXT_BASE_WIDTH * 2) + 2))
                    if uiComponents.SmallButtonColored(ICON.MD_EDIT .. "##editWidgetHoverButton" .. column.id, COLOR.DODGERBLUE) then
                        openColumnEditPopup(popupWindowHandle, column)
                    end
                    uiComponents.ToolTip("Right-click to edit this element")
                end

                if ImGui.IsItemHovered() then
                    hovering[column.id] = true
                else
                    hovering[column.id] = false
                end

                if ImGui.BeginPopup(popupWindowHandle, ImGuiWindowFlags.NoResize) then
                    renderCustomElementEditForm(editing.widget, columns, columnIndex, rowIndex,
                        function()
                            table.remove(columns, columnIndex)
                            save()
                            ImGui.CloseCurrentPopup()
                        end,
                        function()
                            columns[columnIndex] = editing.widget
                            save()
                            ImGui.CloseCurrentPopup()
                        end,
                        ImGui.CloseCurrentPopup
                    )
                    ImGui.EndPopup()
                end

                if columnIndex < #columns then
                    ImGui.NextColumn()
                end
            end

            ImGui.NextColumn()

            if uiComponents.SmallButtonColored(ICON.MD_ADD_BOX .. "##addColumnButton" .. tostring(rowIndex), COLOR.DARKSLATEBLUE) then
                table.insert(row.columns, createWidget())
                save()
            end
            uiComponents.ToolTip(ICON.MD_ADD_BOX .. " Add new widget")
        end

        if #columns <= 0 then
            ImGui.SetCursorPosX(cursorPosX)
            if uiComponents.SmallButtonColored(ICON.MD_ADD_BOX .. " Widget##addColumnButton" .. tostring(rowIndex), COLOR.DARKSLATEBLUE) then
                table.insert(row.columns, createWidget())
                save()
            end
            uiComponents.ToolTip(ICON.MD_ADD_BOX .. " Add new widget")
            ImGui.SameLine()
            if uiComponents.SmallButtonColored(ICON.MD_ADD_BOX .. " Example Widgets##addExampleColumnButton" .. tostring(rowIndex)) then
                ImGui.OpenPopup(EXAMPLES_POPUP_WINDOW_HANDLE)
            end
            renderExamplesPopup()
            uiComponents.ToolTip(ICON.MD_ADD_BOX .. " Add a series of example widgets")
            ImGui.SameLine()
            if uiComponents.SmallButtonColored(ICON.FA_TRASH .. " Remove Row##removeRowButton" .. tostring(rowIndex), COLOR.ORANGERED) then
                table.remove(rows, rowIndex)
                save()
            end
        end

        ImGui.Columns(1)

        ImGui.Separator()

        ImGui.EndGroup()

        if #columns <= 0 then
            renderCustomRowContextMenu(row, rows, rowIndex)
        end
    end

    if ImGui.SmallButton(ICON.MD_ADD_BOX .. " Custom Row##addRowButton") then
        table.insert(rows, {
            id = utils.uuid(),
            columns = {}
        })
        save()
    end

    ImGui.Separator()

    ImGui.EndGroup()
end

return displayCustomElements
