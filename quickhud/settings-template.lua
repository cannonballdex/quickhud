local ICON = require('quickhud.lib.icons')
local utils = require('quickhud.utils')

local settings = {
    version = 6,
    showGMBanner = true,
    gmBannerColor = {1, 0, 0, 1},

    showCharacterInfo = true,
    showCharacterIcon = true,
    characterIcon = ICON.FA_USER_CIRCLE,
    showInvisStatus = true,
    showCharacterLevelAndClass = true,
    invisStatusColor = {0, 0.75, 1, 1},
    showLevitatingStatus = true,
    showLevitatingStatusText = false,
    showMovementStatus = true,
    showMovementStatusText = false,
    showCharacterPrimaryStats = true,
    showCharacterPrimaryStatValues = true,
    showCharacterPrimaryStatPercentage = true,
    showCharacterPrimaryStatsLabel = true,
    showSubscriptionStatus = false,
    showSubscriptionStatusText = true,
    showCombatStateIcon = true,

    groupCharacterAndLocation = true,

    showLocationInfo = true,
    showLocation = true,
    showLocationBookmark = true,
    showLocationRecordedWaypoints = true,

    showExperienceInfo = true,
    showExperience = true,
    showAAExperience = true,
    showMercAAExperience = false,

    showZoneInfo = true,
    showZonePlayerCount = true,
    showZoneBreakdown = true,
    showZoneFullName = true,
    showZoneShortName = true,
    showZoneId = true,

    showLastTellInfo = true,

    showMacroInfo = true,
    showMacroInfoWhenNotActive = true,
    showMacroControls = true,
    showMacroButtonLabels = false,
    showColoredMacroButtons = true,

    showTimeInfo = true,
    showGameTime = true,
    showEarthTime = true,

    showMonitoredTargetInfo = true,
    showMyTargetInfo = true,
    showMATargetInfo = true,
    showAggroIndicator = true,
    showConsiderColor = true,
    showLosIndicator = true,

    showClipboardInfo = false,

    enduranceBarColor = {utils.hex("#A6A800")},
    experienceBarColor = {0.85, 0.65, 0, 0.75},

    windowOpacity = 0.35,
    contentOpacity = 1.0,
    showLineSeparators = true,
    backgroundColor = {0,0,0,1},
    textColor = {1,1,1,1},
    separatorColor = {1,1,1,0.2},

    restingStateBackgroundColor = {0.363, 0.323, 0.213, 1.0},
    combatStateBackgroundColor = {0.418, 0.108, 0.108, 1.0},
    activeStateBackgroundColor = {0.363, 0.323, 0.213, 1.0},
    debuffedStateBackgroundColor = {0.213, 0.363, 0.251, 1.0},
    cooldownStateBackgroundColor = {0.321, 0.321, 0.321, 1.0},
    statBackgroundColorBlend = 1.0,
    paddingMultiplier = 0.85,

    uiElements = {
        { id = 1, order = 1, title = "GM Bar" },
        { id = 2, order = 2, title = "Character Info" },
        { id = 3, order = 3, title = "Character Stats" },
        { id = 4, order = 4, title = "Experience Bars" },
        { id = 5, order = 5, title = "Zone Info" },
        { id = 6, order = 6, title = "Last Tell" },
        { id = 7, order = 7, title = "Macro Status" },
        { id = 8, order = 8, title = "Time" },
        { id = 9, order = 9, title = "Targets" },
        { id = 10, order = 10, title = "Clipboard Contents" },
        { id = 11, order = 11, title = "Group Functions" }
    },

    customElements = {},

    useClassShortNames = true,

    showStatBackgroundColor = true,

    combatStateDefaultColor = {0,0,0,0},

    showTitleBar = false,

    useIndividualSettings = false
}

return settings
