-- By Loki 2022.03.03
-- https://gitlab.com/anonymousbitshifter/quickhud

--- @type Mq
local mq = require('mq')

--- @type ImGui
require 'ImGui'

TEXT_BASE_WIDTH = ImGui.CalcTextSize("A")
TEXT_BASE_HEIGHT = ImGui.GetTextLineHeightWithSpacing()

-- Logging
local log = require('quickhud.lib.Write')
log.prefix = function() return string.format("[%s] Quick HUD", os.date("%Y.%m.%d %H:%M:%S")) end
log.loglevel = 'info'

-- Libraries
local ICON = require('quickhud.lib.icons')
local COLOR = require('quickhud.lib.color')
local utils = require('quickhud.utils')
local services = require('quickhud.services')
local state = require('quickhud.state')
local ui = require('quickhud.quickhudui')
local uiComponents = require('quickhud.uihelpers')
local quickhudsettings = require('quickhud.quickhudsettings')
local linq = require("quickhud.lib.lazylualinq.linq")

-- Services
local appSettingsService = services.AppSettings

-- HUD Window
local QuickHudGUI = function()
    if (not state.openGUI) or (not mq.TLO.Me.CleanName()) then return end
    uiComponents.PushWindowSettingsStyles(state)
    local flags = (not state.settings.showTitleBar) and ImGuiWindowFlags.NoTitleBar or 0
    flags = bit32.bor(flags or 0, state.locked and ImGuiWindowFlags.NoMove or 0, state.locked and ImGuiWindowFlags.NoResize or 0)
    state.openGUI, state.shouldDrawGUI = ImGui.Begin(appSettingsService:GetMainWindowTitle(), state.openGUI, flags)
    if state.shouldDrawGUI then
        if ImGui.GetWindowHeight() == 32 and ImGui.GetWindowWidth() == 32 then
            ImGui.SetWindowSize(460, 177)
        end

        utils.safeCall(displayInfo, "displayInfo()")
        utils.safeCall(quickhudsettings.renderSettingsMenuButton, "renderSettingsMenuButton()")

        if state.locked then
            ImGui.SetWindowPos(state.windowX, state.windowY)
        end
    end
    ImGui.End()
    uiComponents.PopWindowSettingsStyles()
end

function injectWindowLocking()
    local helpText = "Lock to this position."

    if state.locked then
        ImGui.SetWindowPos(state.windowX, state.windowY)
        ImGui.SetWindowSize(state.windowWidth, state.windowHeight)
        helpText = "Unlock from this position."
    end

    local lockButtonText = state.locked and ICON.MD_LOCK or ICON.MD_LOCK_OPEN

    if uiComponents.SmallButtonColored(lockButtonText, {0,0,0,0.35}, nil, nil, state.locked and COLOR.SILVER or COLOR.SILVER) then
        state.locked = not state.locked
        if state.locked then
            state.windowX, state.windowY = ImGui.GetWindowPos()
            state.windowWidth, state.windowHeight = ImGui.GetWindowSize()
        end
    end

    if ImGui.IsItemClicked(ImGuiMouseButton.Right) then
        log.Info("Syncronizing QuickHUD window position across DanNet network.")
        mq.cmd(string.format('/squelch /dge /%s-sync %d %d %d %d %s', 'qhud', state.windowX, state.windowY, state.windowWidth, state.windowHeight, state.locked))
    end

    uiComponents.ToolTip(helpText .. "\n\nRight-click to syncronize the position of QuickHUD windows with other clients on a DanNet network (requires MQ2DanNet plugin)\n\nWhen locked the window cannot be moved or resized and will maintain position when game window is not in focus.")
end

local uiConditions = {
    { id= 1, condition = function(settings) return settings.showGMBanner end },
    { id= 2, condition = function(settings) return settings.showCharacterInfo end },
    { id= 3, condition = function(settings) return settings.showCharacterPrimaryStats end },
    { id= 4, condition = function(settings) return settings.showExperienceInfo end },
    { id= 5, condition = function(settings) return settings.showZoneInfo end },
    { id= 6, condition = function(settings) return settings.showLastTellInfo end },
    { id= 7, condition = function(settings) return settings.showMacroInfo end },
    { id= 8, condition = function(settings) return settings.showTimeInfo end },
    { id= 9, condition = function(settings) return settings.showMonitoredTargetInfo end },
    { id= 10, condition = function(settings) return settings.showClipboardInfo end },
    { id= 11, condition = function(settings) return true end }
}

function displayInfo()
    local shouldInjectLocking = true
    local sortedElements = linq.from(state.settings.uiElements)
        :orderBy(function(element) return element.order end)
        :foreach(function(index, element)
            if uiConditions[element.id].condition(state.settings) then
                if shouldInjectLocking then
                    shouldInjectLocking = false
                    injectWindowLocking()
                    ImGui.SameLine()
                end
                ui.uiRenderers[element.id]()
            end
        end)
end

function syncWindow(x, y, width, height, locked)
    x = tonumber(x or "0")
    y = tonumber(y or "0")
    width = tonumber(width or "0")
    height = tonumber(height or "0")
    locked = locked == "true"

    state.locked = locked
    state.windowX = x
    state.windowY = y
    state.windowWidth = width
    state.windowHeight = height
end

function seedRng()
    math.randomseed(os.clock()*95020209090)
    for i=1,6 do
        math.random(10000, 65000)
    end
end

function initialize()
    appSettingsService:LoadRequiredPlugins()

    seedRng()

    state.settings = appSettingsService:LoadAppSettings()
    state.settingsBuffer = utils.copyTable(state.settings)

    quickhudsettings.register()

    log.Info("Initialized")
end

utils.safeCall(initialize, "initialize()")

mq.imgui.init('QuickHudGUI', QuickHudGUI)
mq.bind('/qhud', function()
    state.openGUI = not state.openGUI
end)

mq.bind(string.format('/%s-sync', 'qhud'), syncWindow)

local terminate = false
mq.bind('/qhudquit', function() terminate = true end)

state.commands = {}
local function dispatch(command)
    table.insert(state.commands, command)
end
state.dispatch = dispatch

function processCommands()
    local commandsToProcess = utils.copyTable(state.commands)
    state.commands = {}
    for index, command in ipairs(commandsToProcess) do
        log.Debug(string.format("Processing Command #%d of %d", index, #commandsToProcess))
        command()
    end
end

while not terminate do
    utils.safeCall(processCommands, "processCommands()")

    mq.delay(150)
end
