# Quick HUD

Quick HUD is a Lua addon for use with MacroQuest that enables some handy information and functionality at a glance.

![Quick HUD Preview](resources/quickhud.png)

## Motivation

When I realized that the Lua implementation in MacroQuest has UI capabilities baked in I was inspired to re-interpret a MQ2HUD configuration to display some heads up data regaring character/zone/macro info.

This quickly devolved into a sandbox where I was learning Lua and ImGui and experimenting with the features instead of keeping things simple. Since I am not so familiar with Lua I am sure there are a _lot_ of areas for improvement both in code quality and efficiency. I am always open to criticism and suggestions for improvement.

I would like to sincerely thank people in the community for the development efforts they have made that I have taken a dependency on, inspiration from or outright copied. Thank you: Knightly, Sic, SpecialEd, jb321, aquietone, Coldblooded, brainiac and many more.

This was the MQ2HUD

![Quick HUD Preview](resources/old-hud.png)

----

## Usage

### Commands
- Load the addon
    - `/lua run quickhud`
- Toggle the HUD window
    - `/qhud`
- Toggle settings window
    - `/qhudsettings`
- Quit the HUD
    - `/qhudquit` or `/lua stop quickhud`

### Plugin Dependencies
- MQ2Nav
    - The addon will try to load this on startup if it is not already loaded.

----

## Features

### GM Banner
![Quick HUD Preview](resources/quickhud-gm-banner.png)
- If a GM is detected in the zone then it displays a bright (configurable) banner showing the GM info including:
    - Name
    - Level
    - Race
    - Class
    - Distance
    - Health Percentage 

### Character
- Display invisibility status when invisible
- Display character level and class
- Display levitation status with capability to remove levitation effects
- Display Health / Mana / Endurance bars relevent to player class
- Display Level / AA Experience / Merc AA XP bars

### Location
- Display location with ability to copy to clipboard
- Ability to bookmark waypoints using MQ2Nav (`/rwp`)
    - ![Quick HUD Preview](resources/quickhud-bookmark.png)
- Ability to click to navigate to zone waypoints using MQ2Nav
    - I found that I would consistently forget what I had named a waypoint with `/rwp` and I am far too lazy to open the ini file so I baked in this utility.
    - ![Quick HUD Preview](resources/quickhud-nav.png)

### Zone
- Display the number of characters in the zone
- Display a breakdown of players in zone grouped by guild and ordered by player count in that guild
    - ![Quick HUD Preview](resources/quickhud-zone-breakdown.png)
- Display the Full name / Short name / Id of zone

### Macro
- Display the currently loaded macro
- Show macro status (Running / Pause / Not Loaded)
- Provide controls to Pause / Resume / End macro

### Time
- Display the time for Norrath and/or Local Earth time

### Target
- Display Target and/or MA Target information including
    - Name
    - Level
    - Race
    - Class
    - Distance
    - Health Percentage
- Capability to target mob via left-clicking target info bar
- Capability to navigate to target via right right-clicking target info bar

### Needlessly Configurable
- The HUD is pretty busy by default and might serve as an eyesore more than a beneficial utility so it can be almostly needlessly configured to suit your preferences.
    - Example of a more compact configuration
    - ![Quick HUD Preview](resources/quickhud-configured.png)
- The HUD can be configured to the point where it has absolutely no value so even if you don't like it there is an option for you!
- Configuration can be shared across accounts or allow individual characters to retain their own settings.
- ![Quick HUD Preview](resources/quickhud-settings.png)


----